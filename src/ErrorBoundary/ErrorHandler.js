import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        state = {
            text: '',
            type: null,
            title: null,
            showModal: false
        }
        signal = axios.CancelToken.source();
        componentWillUnmount = () => this.signal.cancel(`cancelado error handler`);
        componentDidMount() {
            try {
                axios.interceptors.request.use(req => {
                    this.setState({ text: '' });
                    return req;
                });

                axios.interceptors.response.use(res => res, error => {
                    this.setState({ text: error || "Ha ocurido un ERROR!", showModal: true });
                });

            } catch (error) {
                if (axios.isCancel(error)) { console.log(error.message); }
            }
        }



        errorConfirmedHandler = () => {
            this.setState({ error: null, showModal: false });
            return this.props.history.push("/dashboard/")
        }

        render() {
            return (
                <div>
                    <Modal show={this.state.showModal} >
                        <Modal.Header style={{ backgroundColor: '#4e73df', color: 'white' }}>
                            <Modal.Title>{`ERROR`}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            {"Ha ocurrido un error obteniendo información"}
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="light btn-sm" onClick={this.errorConfirmedHandler}>  Cerrar </Button>
                        </Modal.Footer>
                    </Modal>
                    <WrappedComponent {...this.props} />
                </div>
            )
        }
    }
}
export default withErrorHandler;