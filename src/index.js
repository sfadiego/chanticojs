import React from 'react';

import ReactDOM from 'react-dom';
import App from './App';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { BrowserRouter } from 'react-router-dom'
import {
    faCoffee,
    faCalculator,
    faTag,
    faPercentage,
    faUtensils,
    faSearch,
    faPlus,
    faMinus,
    faBell,
    faTachometerAlt,
    faChevronRight,
    faArrowLeft,
    faCogs,
    faCheck,
    faWrench,
    faTable,
    faShoppingCart,
    faShoppingBasket,
    faTimes,
    faCreditCard,
    faDonate,
    faBars,
    faPizzaSlice,
    faClock,
    faHamburger,
    faCheckCircle,
    faChevronLeft,
    faEdit, faBackspace,
    faSpinner, faClipboard,
    faPrint,
    faUser,
    faMoneyBillAlt,
    faSignOutAlt,
    faTrash, faPlusCircle, faHandHoldingUsd, faList, faChartPie
} from '@fortawesome/free-solid-svg-icons'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import './Resources/template/sb-admincss.css';
import './Resources/template/customcss.css';
import 'jquery/dist/jquery.js';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux'
import auth from './Componentes/Core/Store/reducers/auth';
import productos from './Componentes/Core/Store/reducers/adminProductos';
import axios from 'axios';
import * as GLOBAL from './Componentes/Core/Constantes'

library.add(fab, faCalculator, faSignOutAlt, faUser, faPrint, faMoneyBillAlt, faList, faSpinner, faChartPie, faPercentage, faCoffee, faUtensils, faSearch, faTag, faPlus, faMinus, faBell, faTachometerAlt, faChevronLeft, faChevronRight, faArrowLeft, faCheck, faCogs, faWrench, faTable, faShoppingCart, faShoppingBasket, faTimes, faCreditCard, faDonate, faBars, faPizzaSlice, faClock, faHamburger, faCheckCircle, faEdit, faTrash, faPlusCircle, faClipboard, faBackspace, faHandHoldingUsd);

axios.defaults.baseURL = GLOBAL.PATH_URL;
axios.interceptors.request.use(request => request, error => Promise.reject(error));

const logger = store => {
    return next => {
        return action => {
            // console.log('[Middleware] Dispatching ', action)
            const result = next(action);
            // console.log('[Middleware]', store.getState())
            return result;
        }
    }
}
const composeEnhancers = process.env.NODE_ENV === 'development'  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;
const rootReducers = combineReducers({
    auth: auth,
    productos: productos
})
const store = createStore(rootReducers, composeEnhancers(applyMiddleware(logger, ReduxThunk)));

ReactDOM.render(<Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
