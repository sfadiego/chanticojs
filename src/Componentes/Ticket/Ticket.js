import React, { Component } from 'react'
import img from '../../Resources/img/chantico_ticket.png';
import * as Funciones from '../../Componentes/Core/Funcions';
import './StyleTicket.css';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import LoadingComponent from '../Elementos/LoadingComponent';
let GLOBAL = require("../Core/Constantes");

export default class Ticket extends Component {
    constructor(props, context) {
        super(props, context);
        this.state ={
            productos:[],
            total:0,
            estatus:[],
            pedidoInfo:[],
            subtotal:0,
            descuento:0,
            comandaid:null,
            isLoading:true,
            showButtons:false
        }
    }
    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount(){
        try {
            const comandaid = this.props.match.params.comandaid;
            if(!comandaid) this.props.history.push("/mesas");
            Funciones.getDataAxios('catestatuspedido', this.signal.token)
            .then(success => this.setState({estatus:success, comandaid:comandaid}))
            .catch(error=> console.log(GLOBAL.API_CANCELADA,error.message));
            this.getDataComanda();
            
        } catch (error) {
            if (axios.isCancel(error)) { console.log(GLOBAL.API_CANCELADA); } 
        }
    }

    getDataComanda = async ()=>{
        try {
            let comandaid = this.props.match.params.comandaid;
            let comanda = await Funciones.getDataAxios('comandaByPedidoId/' + comandaid, this.signal.token)
            let data = comanda.data.comanda
            await this.getExtras(data);
            let pedidoInfo = comanda.data.pedido;
            this.setState({productos:data, 
                            isLoading:false, 
                            pedidoInfo:pedidoInfo
                        },()=>{
                            this.pedidofinalizado();
                            this.calculaTotal(data);
                        });            
        } catch (error) {
            if (axios.isCancel(error)) { console.log(GLOBAL.API_CANCELADA); }  
        }
    }

    getExtras = async (data)=> {
        const comandaid = this.props.match.params.comandaid;
        let extras = await Funciones.getDataAxios(`comanda/extras/${comandaid}`);
        let productos = data;
        productos.map(producto =>{
          let obj_Extra = extras.filter(item => (item.key === producto.key && item.unique === producto.unique))
          return producto.extras = [].concat(obj_Extra);
        });      
      }

    calculaTotal(obj){
        let productos = obj;
        let total = 0;
        productos.map(async element => {
            let total_extras = await Funciones.calculaExtras(element.extras).then((data)=> { return data; }, (error)=> error);
            let calculaTotal = Funciones.calculaTotal(element, total, total_extras);
            total = calculaTotal.total
            if (this.state.descuento) {
                return this.setState({
                    total: Funciones.calculaTotalConDescuento(calculaTotal.subtotal,this.state.descuento),
                    subtotal: calculaTotal.subtotal,
                    descuento: parseInt(this.state.descuento)
                });
            }

            return this.setState({ total: calculaTotal.total, subtotal: calculaTotal.subtotal });
        });
    }

    pedidofinalizado(){
        let getstatus = Funciones.getStatusFinalizado(this.state.estatus);
        let data = this.state.pedidoInfo.estatus_id[0]._id;
        let status = getstatus._id;
        let showButtons = (status === data) ? false : true;
        this.setState({showButtons:showButtons});
    }

    render() {
        let productos = this.state.productos ? this.state.productos : [];
        let date = Funciones.formatDate();
        let total = this.state.total;
        let descuento = this.state.descuento;
        let subtotal = this.state.subtotal;
        return (
            <div className="row color444444">
                <div className="col-md-4 text-center">
                    <button className="btn btn-success col-md-4 br-0" onClick={()=> window.print() }><FontAwesomeIcon icon="print"></FontAwesomeIcon> Imprimir</button>
                    <br></br>
                </div>
                {
                    this.state.isLoading 
                    ? <div id="ticket" className="col-md-4 ticket">
                        <LoadingComponent /> 
                    </div>
                    :
                    <div id="ticket" className="col-md-4 ticket">
                        <div className="logo text-center">
                            <img src={img} className="img-fluid" alt="Logotipo"></img>
                        </div>
                        <div className="w-100 pt-1"> Queseria Colima </div>
                        <div className="w-100 pt-0">{date}</div> 

                        <table className="w-100 mt-2">
                            <thead>
                                <tr>
                                    <th className="cantidad">CANTIDAD</th>
                                    <th className="producto">PRODUCTO</th>
                                    <th className="precio">$$</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    productos.map((item ,key) =>{
                                        let htmlextra= Funciones.obtenerExtrasTicket(item);
                                        return <tr key={key}>
                                            <td className="cantidad">{`${item.cantidad}`}</td>
                                            <td className="producto">
                                                <div> {item.producto} </div>
                                                {
                                                    htmlextra.length > 0 ? 
                                                    htmlextra.map((elemento,key2) =>{
                                                       return <div key={key2} className="row ml-2">
                                                           <div className="col-12 col-sm-12 text-xs" > 
                                                                { elemento.nombre } ${elemento.precio}  
                                                            </div>
                                                        </div>
                                                    }): ""
                                                }
                                            </td>
                                            <td className="precio">
                                                ${ Funciones.calculaExtrasYproductoxRow(item) }
                                            </td>
                                        </tr>
                                    })
                                }
                            </tbody>
                        </table>
                        <table className="w-100 mt-3">
                            <thead>
                                <tr>
                                    <th className="cantidad"></th>
                                    <th className="producto"></th>
                                    <th className="precio"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className="ticket-totales">
                                    <td></td>
                                    <td>DESCUENTO</td>
                                    <td className="cantidad">{descuento}%</td>
                                </tr>
                                <tr className="ticket-totales">
                                    <td></td>
                                    <td>SUBTOTAL</td>
                                    <td className="cantidad">${subtotal}</td>
                                </tr>
                                <tr className="ticket-totales">
                                    <td className="cantidad"></td>
                                    <td className="producto">TOTAL</td>
                                    <td className="precio">${total}</td>
                                </tr>
                            </tbody>
                        </table>
                        <p className="centrado">¡GRACIAS POR SU COMPRA! <br></br> Chantico.qsr</p>
                    </div>
                }
                <div className="col-md-4"></div>
            </div>
        )
    }
}
