import React from 'react';
import Sidebar from '../Template/Sidebar';
const Layout = (props) => (
    props.isAuthenticated ?
    <div id="wrapper">
        <Sidebar/>
        {props.children}
    </div> : props.children
);

export default Layout;