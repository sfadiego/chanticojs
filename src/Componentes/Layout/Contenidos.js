import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import ListaPedidos from '../Comandas/ListaPedidos';
import Informes from '../Administracion/Informes'
import ListadoProductos from '../Administracion/Productos/ListadoProducto';
import AdministrarProducto from '../Administracion/Productos/AdministrarProducto';
import CatalogoEstatus from '../Administracion/Estatus/CatalogoEstatus';
import NuevoEstatus from '../Administracion/Estatus/NuevoEstatus';
import ListadoCategorias from '../Administracion/Categorias/Listado';
import NuevaCategoria from '../Administracion/Categorias/Categorias';
import Comanda from '../Comanda';
import Ticket from '../Ticket/Ticket';
import ListadoExtras from '../Administracion/Extras/ListadoExtras';
import Sabores from '../Administracion/Extras/Sabores/Sabores';
import Aderezos from '../Administracion/Extras/Aderezos/Aderezo';
import Proteinas from '../Administracion/Extras/Proteinas/Proteinas';
import Bebidas from '../Administracion/Extras/Bebidas/Bebidas';
import SaboresDulces from '../Administracion/Extras/SaborDulces/SaboresDulces';
import RegistraEditaSaborDulce from '../Administracion/Extras/SaborDulces/RegistraEdita';
import RegistrarEditaAderezo from '../Administracion/Extras/Aderezos/RegistraEdita';
import RegistrarEditaBebidas from '../Administracion/Extras/Bebidas/RegistraEdita';
import RegistrarEditaProteina from '../Administracion/Extras/Proteinas/RegistraEdita';
import RegistrarEditaSabores from '../Administracion/Extras/Sabores/RegistraEdita';
import PedidosTerminados from '../Comandas/PedidosTerminados'
import DetalleTerminados from '../Comandas/DetalleTerminados';
import LayoutContenidos from './LayoutContenidos'
import Logout from '../Login/Logout'
import Cerrarcaja from '../Comandas/Cerrarcaja'
import Bienvenida  from '../Bienvenida'
import * as actions from '../../Componentes/Core/Store/actions/index'
class Contenidos extends Component {

    componentDidMount() {
        let fechaActual = new Date();
        let cerradaHasta = new Date(this.props.cerradaHasta)
        if (this.props.cajaCerrada && fechaActual >= cerradaHasta) {
            this.props.abrirCaja(this.props.tienda)
            return <Redirect to="/cerrarcaja"></Redirect>;
        }
    }
    render() {
        let component = this.props.cajaCerrada ? Cerrarcaja : Bienvenida;
        return (
            <LayoutContenidos>
                <Route path="/" exact component={component}></Route>
                <Route path="/dashboard" exact component={Bienvenida}></Route>
                <Route path="/logout" exact component={Logout}></Route>
                <Route path="/cerrarcaja" exact component={Cerrarcaja}></Route>
                <Route path="/mesas" exact component={ListaPedidos}></Route>
                <Route path="/comanda/:comandaid" exact component={Comanda}></Route>
                <Route path="/imprimir/ticket/:comandaid" exact component={Ticket}></Route>
                <Route path="/comandas/terminadas" exact component={PedidosTerminados}></Route>
                <Route path="/revisar/comanda/:id" exact component={DetalleTerminados}></Route>
                <Route path="/admin/informes" exact component={Informes}></Route>
                <Route path="/admin/productos/:id?" exact component={AdministrarProducto}></Route>
                <Route path="/admin/catalogo/productos" exact component={ListadoProductos}></Route>
                <Route path="/admin/catalogo/categorias" exact component={ListadoCategorias}></Route>
                <Route path="/admin/catalogo/categoria/nueva" exact component={NuevaCategoria}></Route>
                <Route path="/admin/complementos" exact component={ListadoExtras}></Route>
                <Route path="/admin/complementos/sabor" exact component={Sabores}></Route>
                <Route path="/admin/sabor/:id" exact component={RegistrarEditaSabores}></Route>
                <Route path="/admin/crear/sabor" exact component={RegistrarEditaSabores}></Route>
                <Route path="/admin/complementos/aderezo" exact component={Aderezos}></Route>
                <Route path="/admin/aderezo/:id" exact component={RegistrarEditaAderezo}></Route>
                <Route path="/admin/crear/aderezo" exact component={RegistrarEditaAderezo}></Route>
                <Route path="/admin/complementos/proteina" exact component={Proteinas}></Route>
                <Route path="/admin/proteina/:id" exact  component={RegistrarEditaProteina}></Route>
                <Route path="/admin/crear/proteina" exact component={RegistrarEditaProteina}></Route>
                <Route path="/admin/complementos/bebidas" exact component={Bebidas}></Route>
                <Route path="/admin/bebidas/:id" exact component={RegistrarEditaBebidas}></Route>
                <Route path="/admin/crear/bebidas" exact component={RegistrarEditaBebidas}></Route>
                <Route path="/admin/catalogo/estatus" exact component={CatalogoEstatus}></Route>
                <Route path="/admin/catalogo/estatus/nuevo" exact component={NuevoEstatus}></Route>
                <Route path="/admin/complementos/saboresdulces" component={SaboresDulces}></Route>
                <Route path="/admin/crear/sabordulce" exact component={RegistraEditaSaborDulce}></Route>
                <Route path="/admin/sabordulce/:id" exact component={RegistraEditaSaborDulce}></Route>
            </LayoutContenidos>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        cajaCerrada: state.auth.cajaCerrada,
        cerradaHasta: state.auth.cerradaHasta,
        tienda: state.auth.tienda,
        role: parseInt(state.auth.role)
    }
}

let mapDispatchToprops = (dispatch) => {
    return {
        abrirCaja: (tienda) => dispatch(actions.openSales(tienda))
    }
}

export default connect(mapStateToProps, mapDispatchToprops)(Contenidos);