import React from 'react';
import Footer from '../Template/Footer';
import TopbarContainer from '../../Componentes/Topbar/TopbarContainer'
const LayoutContenidos = (props) => (
    <div id="content-wrapper" className="d-flex flex-column">
        <div id="content">
            <TopbarContainer></TopbarContainer>
            {props.children}
        </div>
        <Footer></Footer>
    </div>
)

export default LayoutContenidos;