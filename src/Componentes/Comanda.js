import React, { Component } from 'react'
import Productos from './Elementos/Productos';
import NoProducto from './Elementos/NoProducto'
import CategoriasTabs from './Elementos/CategoriasTabs';
import { Animated } from "react-animated-css";
import NoPedidos from './Elementos/Comandas/NoPedidos';
import * as Funciones from './Core/Funcions'
import Total from './Elementos/Comandas/Total'
import './Elementos/Comandas/Styles/Styles.css'
import axios from 'axios';
import ExtrasProductos from './Comandas/ExtrasProductos';
import AlertComponent from './AlertComponents/AlertComponent';
import ShowTotalPago from './Elementos/Comandas/ShowTotalPago'
import Calculadora from './Core/Calculadora/Calculadora';
import LoadingComponent from './Elementos/LoadingComponent'
import RenderItems from './Elementos/Comandas/RenderItems';
import EditarxItem from './Comandas/EditarxItem';
import AgregarDescuento from './Comandas/AgregaDescuento';
import * as viewsHandler from './Elementos/ViewsHandler'
import { updateObject } from './Core/utility'
let GLOBAL = require("./Core/Constantes");

export default class Comanda extends Component {

  constructor(props) {
    super(props)
    this.state = {
      total: 0,
      descuento: 0,
      subtotal: 0,
      productos: [],
      pedidoInfo: null,
      productosComanda: [],
      estatus: [],
      selectedItemId: null,
      validateExtrasObject: null,
      productoSeleccionadoTemp: [],
      statusCode: null,
      modal: {
        title: '',
        icon: GLOBAL.MODAL_SUCCESS,
        type: GLOBAL.MODAL_SUCCESS,
        text: ''
      },
      totalCambio: 0,
      comandaid: null,
      views: { productos: true },
      showmodal: false,
      isLoading: true,
    }
    this.borrarElementoRowHandler = this.borrarElementoRowHandler.bind(this);
    this.masUnElementoHandler = this.masUnElementoHandler.bind(this);
    this.menosUnElementoHandler = this.menosUnElementoHandler.bind(this);
    this.onchangeGetDataCategoria = this.onchangeGetDataCategoria.bind(this);
    this.onclickAgregaProductos = this.onclickAgregaProductos.bind(this);
    this.pagarComanda = this.pagarComanda.bind(this);
    this.guardarPedido = this.guardarPedido.bind(this);
    this.handleExtra = this.handleExtra.bind(this);
    this.closeExtras = this.closeExtras.bind(this);
    this.closeCalculator = this.closeCalculator.bind(this);
    this.agregarDescuento = this.agregarDescuento.bind(this);
    this.handleModal = this.handleModal.bind(this);
    this.onEqual = this.onEqual.bind(this);
    this.procesar = this.procesar.bind(this);
    this.handleDetalleItem = this.handleDetalleItem.bind(this);
    this.closeViewPorcentaje = this.closeViewPorcentaje.bind(this);
    this.modificarItemPorcentaje = this.modificarItemPorcentaje.bind(this);
    this.handleAgregarAcomandaNuevo = this.handleAgregarAcomandaNuevo.bind(this);
    this.calculaTotalDescuento = this.calculaTotalDescuento.bind(this);
    this.showCalculadora = this.showCalculadora.bind(this);
    this.editarExtrasEnComanda = this.editarExtrasEnComanda.bind(this);

  }

  // Cargar informacion
  signal = axios.CancelToken.source();
  componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);
  componentDidMount() {
    this.loadComponentInfo();
  }

  loadComponentInfo = async () => {
    try {
      const comandaid = this.props.match.params.comandaid;
      let relComandaPedido = await axios.get('comandaByPedidoId/' + comandaid, { cancelToken: this.signal.token });
      let comanda = relComandaPedido.data.data.comanda;
      let pedido = relComandaPedido.data.data.pedido;
      let ordenado = (comanda.length > 0) ? true : false;
      if (!pedido) { return this.props.history.push("/mesas/"); }
      let pedidoInfo = await Funciones.getDataAxios(`pedidos/${comandaid}`, this.signal.token);
      let updateObjectComanda = updateObject(this.state, {
        pedidoOrdenado: ordenado,
        pedidoInfo: pedidoInfo,
        comandaid: pedido._id,
        descuento: pedido.descuento,
        productosComanda: comanda,
        isLoading: false
      })

      this.setState(updateObjectComanda);
      this.getExtras();

      let estatusPedido = await axios.get('catestatuspedido', { cancelToken: this.signal.token });
      let catalogo = estatusPedido.data;
      this.setState({ estatus: catalogo }, () => this.calculaTotal(this.state.productosComanda));
      this.validateStatus(pedidoInfo);
    } catch (error) {
      if (axios.isCancel(error)) { console.log(GLOBAL.API_CANCELADA, error.message); }
    }
  }

  getExtras = async () => {
    const comandaid = this.props.match.params.comandaid;
    let extras = await Funciones.getDataAxios(`comanda/extras/${comandaid}`, this.signal.token);
    let productos = this.state.productosComanda;
    productos.map(producto => {
      let obj_Extra = extras.filter(item => (item.key === producto.key && item.unique === producto.unique))
      return producto.extras = [].concat(obj_Extra);
    });
  }

  validateStatus = async (dataActual) => {
    let statusocupado = this.state.estatus.filter(data => data.estatus === GLOBAL.CAT_ESTATUS.OCUPADO);
    let statusFinalizado = this.state.estatus.filter(data => data.estatus === GLOBAL.CAT_ESTATUS.FINALIZADO);
    let actual_status = dataActual.estatus_id[0];
    if (statusocupado[0]._id === actual_status || statusFinalizado[0]._id === actual_status) {
      this.soloLectura()

    }
  }

  soloLectura() {
    let readonly = viewsHandler.viewHandler(GLOBAL.VIEWS.READONLY);
    let update = updateObject(this.state, {
      views: readonly,
      isLoading: true,
    })
    this.setState(update);
  }

  calculaTotal = async (obj) => {
    if (!obj) {
      return false;
    } else if (this.state.productosComanda.length === 0) {
      let calculatotal = updateObject(this.state, {
        total: 0,
        subtotal: 0,
        descuento: 0
      })
      this.setState(calculatotal);
    }

    let productos = obj;
    let total = 0;
    productos.map(async element => {
      let total_extras = await Funciones.calculaExtras(element.extras).then((data) => { return data; }, (error) => error);
      let calculaTotal = Funciones.calculaTotal(element, total, total_extras);
      total = calculaTotal.total;
      if (this.state.descuento) {
        return this.setState({
          total: Funciones.calculaTotalConDescuento(calculaTotal.total, this.state.descuento),
          subtotal: calculaTotal.subtotal,
          descuento: parseInt(this.state.descuento)
        });
      }
      return this.setState({
        total: calculaTotal.total,
        subtotal: calculaTotal.subtotal
      });

    });

  }

  calculaTotalDescuento(porcentajeAdescontar = 0) {
    var porcentaje = parseInt(porcentajeAdescontar);
    let closeItemDetail = viewsHandler.viewHandler(GLOBAL.VIEWS.PRODUCTOS);
    this.setState({
      descuento: porcentaje,
      isLoading: false,
      views: closeItemDetail
    }, () => this.calculaTotal(this.state.productosComanda));
  }

  masUnElementoHandler(data) {
    let index = this.state.productosComanda.findIndex(item => item.unique === data.unique);
    let productosComanda = this.state.productosComanda;
    productosComanda[index].cantidad = data.cantidad + 1;
    this.setState({ productosComanda: productosComanda }, () => {
      this.calculaTotal(this.state.productosComanda);
    });
  }

  menosUnElementoHandler(data) {
    let index = this.state.productosComanda.findIndex(item => item.unique === data.unique);
    let productosComanda = this.state.productosComanda;
    productosComanda[index].cantidad = data.cantidad - 1;
    this.setState(productosComanda, () => {
      this.calculaTotal(this.state.productosComanda);
    });
  }

  borrarElementoRowHandler(data) {
    const productos = this.state.productosComanda.filter(item => item.unique !== data.unique);
    this.setState({ productosComanda: [...productos] }, () => this.calculaTotal(this.state.productosComanda));
  }

  handleModal(text = GLOBAL.SUCCES, type = GLOBAL.MODAL_SUCCESS, title = GLOBAL.MODAL_SUCCESS, showmodal = true, statusCode = null) {
    let dataOptions = updateObject(this.state.modal, {
      text: text,
      type: type,
      title: title
    })

    return this.setState({
      modal: dataOptions,
      showmodal: showmodal,
      isLoading: true,
      statusCode: statusCode
    });

  }

  guardarPedido() {
    if (this.state.productosComanda.length === 0) {
      return this.handleModal(GLOBAL.ITEMS_REQUIRED, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, false);
    } else {

      let statusocupado = this.state.estatus.filter(data => data.estatus === GLOBAL.CAT_ESTATUS.PROCESO);
      let comandaid = this.state.comandaid;
      let idstatus = statusocupado[0];


      let objComanda = {
        data: this.state.productosComanda,
        pedido: {
          id: comandaid,
          total: this.state.total,
          subtotal: this.state.subtotal,
          status: idstatus._id,
          descuento: this.state.descuento,
        }
      };

      axios({
        method: GLOBAL.POST,
        url: 'comandapedido/multiple',
        data: objComanda
      }).then(response => this.handleModal(response.data.message || GLOBAL.SUCCES, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200))
        .catch(error => this.handleModal(GLOBAL.ERROR_SAVING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, false));

    }
  }

  onchangeGetDataCategoria(id) {
    if (!id) { return; }
    this.setState({ isLoading: true });
    axios.get('findByCategoriaId/' + id)
      .then(response => this.setState({ productos: response.data, isLoading: false }))
  }

  editarExtrasEnComanda(data) {
    return this.onclickAgregaProductos(data, true);
  }

  onclickAgregaProductos(data, isToEdit = false) {
    data.isToEdit = isToEdit;
    axios.get(`tieneextrasitem/${data.key}`, { cancelToken: this.signal.token }).then(success => {
      let response = success.data;
      if (response.valido) {
        let showExtra = viewsHandler.viewHandler(GLOBAL.VIEWS.EXTRAS);
        let productoExtra = {
          ...this.state.productoSeleccionadoTemp,
          producto: data,
          extras: data.extras ? [].concat(data.extras) : []
        }
        return this.setState({
          views: showExtra,
          selectedItemId: null,
          validateExtrasObject: response,
          productoSeleccionadoTemp: productoExtra
        }, () => this.setState({ selectedItemId: data.key }))
      } else {
        if (!isToEdit) {
          this.handleAgregarAComanda(data);
        }
      }
    });
  }

  handleAgregarAComanda(data) {
    data.pedido_id = this.state.comandaid;
    let index = this.state.productosComanda.findIndex(item => item.key === data.key);
    if (index === -1) {
      return this.addItemToArray(data);
    }
    return this.sumarElement(index);
  }

  handleAgregarAcomandaNuevo(data) {
    data.pedido_id = this.state.comandaid;
    return this.addItemToArray(data);
  }

  sumarElement(index) {
    let productos_actuales = [...this.state.productosComanda];
    productos_actuales[index].cantidad = productos_actuales[index].cantidad + 1

    this.setState({ productosComanda: productos_actuales }, () => this.calculaTotal(this.state.productosComanda));
  }

  addItemToArray(data) {
    const newObject = [data, ...this.state.productosComanda];
    return this.setState({ productosComanda: newObject }, () => this.calculaTotal(this.state.productosComanda));
  }

  addExtraToArray(productos_actuales) {
    this.setState({
      productosComanda: productos_actuales
    }, () => this.calculaTotal(this.state.productosComanda));
  }

  pagarComanda() { this.pagar(); }

  pagar = async () => {
    let id = this.state.comandaid;
    let statusocupado = this.state.estatus.filter(data => data.estatus === GLOBAL.CAT_ESTATUS.OCUPADO);
    let objComanda = {
      data: this.state.productosComanda,
      pedido: {
        id: id,
        total: this.state.total,
        subtotal: this.state.subtotal,
        status: statusocupado[0]._id,
        descuento: this.state.descuento,
      }
    };
    let calculadora = viewsHandler.viewHandler(GLOBAL.VIEWS.CALCULADORA);
    await Funciones.pagarComanda(objComanda);
    await Funciones.requestAxios("pedidos/ocupado/" + id, GLOBAL.PUT, { estatus_id: statusocupado[0]._id })
      .then(success => this.setState({
        views: calculadora,
        isLoading: true,
      }))
      .catch(error => this.handleModal(GLOBAL.ERROR_UPDATING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 500));
  }

  showCalculadora() {
    let calculadora = viewsHandler.viewHandler(GLOBAL.VIEWS.CALCULADORA);
    this.setState({
      views: calculadora,
      isLoading: true,
    });
  }

  finalizar = async () => {
    let id = this.state.comandaid;
    let statusoFinalizado = this.state.estatus.filter(data => data.estatus === GLOBAL.CAT_ESTATUS.FINALIZADO);
    await Funciones.requestAxios("pedidos/ocupado/" + id, GLOBAL.PUT, { estatus_id: statusoFinalizado[0]._id })
      .then(success => this.props.history.push("/mesas/"))
      .catch(error => this.handleModal(GLOBAL.ERROR_UPDATING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 500));
  }

  onEqual(data) {
    let toPositive = data * -1;
    let totalPago = viewsHandler.viewHandler(GLOBAL.VIEWS.TOTAL_PAGOS);
    this.setState({
      views: totalPago,
      isLoading: true,
      totalCambio: toPositive,
    });
  }

  handleExtra(item_extra, isToEdit = false, uniqueId) {
    if (isToEdit) {
      let index = this.state.productosComanda.findIndex(item => item.unique === uniqueId);
      let productosComanda = this.state.productosComanda;
      let producto = productosComanda[index];
      producto.extras = [].concat(item_extra);
      this.closeExtras();
      return this.setState({ productosComanda: productosComanda }, () => this.calculaTotal(this.state.productosComanda));

    } else {
      let data = this.state.productoSeleccionadoTemp;
      let producto = data.producto;
      producto.extras = item_extra;
      const newObject = [data.producto, ...this.state.productosComanda];
      this.closeExtras();
      return this.setState({ productosComanda: newObject }, () => this.calculaTotal(this.state.productosComanda));
    }
  }

  procesar() {
    this.setState({ isLoading: true });
    this.finalizar();
  }

  closeExtras() {
    let productos = viewsHandler.viewHandler(GLOBAL.VIEWS.PRODUCTOS);
    this.setState({
      productoSeleccionadoTemp: [],
      selectedItemId: null,
      views: productos
    });
  }

  closeCalculator() {
    let productos = viewsHandler.viewHandler(GLOBAL.VIEWS.PRODUCTOS);
    this.setState({
      views: productos,
      isLoading: false,
    });
  }

  agregarDescuento() {
    let showItemDetail = viewsHandler.viewHandler(GLOBAL.VIEWS.DESCUENTO_TOTAL);
    this.setState({ isLoading: true, views: showItemDetail })
  }

  confirmModal() {
    this.setState({ showmodal: false, isLoading: false });
    if (this.state.statusCode === 200) { return this.props.history.push('/mesas/'); }
    else if (this.state.statusCode === 500) { return this.props.history.push('/mesas/'); }
  }

  handleDetalleItem(data) {
    let index = this.state.productosComanda.findIndex(item => item.unique === data.unique);
    let productosComanda = this.state.productosComanda;
    let cantidad_actual = productosComanda[index].cantidad;
    let producto = productosComanda[index].producto;
    let precioUnitario = productosComanda[index].precio;
    let extras = productosComanda[index].extras;
    let selected = [{
      index: index,
      descuento: 0,
      cantidad: cantidad_actual,
      producto: producto,
      precioUnitario: precioUnitario,
      extras: extras
    }];
    let showItemDetail = viewsHandler.viewHandler(GLOBAL.VIEWS.DESCUENTO);
    this.setState({ productoSeleccionadoTemp: selected, isLoading: true, views: showItemDetail })
  }

  modificarItemPorcentaje(item) {
    let comanda = this.state.productosComanda;
    let index = item.index;
    let comandaActual = comanda[index];
    let data = {
      ...comandaActual,
      descuento: item.descuento,
    };
    comanda[index] = data;
    let closeItemDetail = viewsHandler.viewHandler(GLOBAL.VIEWS.PRODUCTOS);
    this.setState({
      views: closeItemDetail,
      isLoading: false,
      productosComanda: comanda
    }, () => this.calculaTotal(this.state.productosComanda));
  }

  closeViewPorcentaje() {
    let closeItemDetail = viewsHandler.viewHandler(GLOBAL.VIEWS.PRODUCTOS);
    this.setState({ views: closeItemDetail, isLoading: false });
  }

  render() {
    const containercomandasrow = { 'minHeight': 'calc(100vh - 9rem)' }
    let nombre_pedido = this.state.pedidoInfo ? this.state.pedidoInfo.nombre_pedido : '';
    let isloading = (this.state.isLoading ||
      Object.keys(this.state.productosComanda).length === 0 ||
      this.state.views.calculadora ||
      this.state.views.extras) ? true : false;
    let calculadora = null;
    if (this.state.views.calculadora) {
      calculadora = <Calculadora
        total={this.state.total}
        handleModal={this.handleModal}
        onEqual={this.onEqual}
        imprimir={this.imprimir}
        comandaid={this.state.comandaid}
        isVisible={this.state.views.calculadora} />
    }
    return (
      <div className="container-fluid pl-0 main-container-comadas">
        <div className="row" style={containercomandasrow}>
          <div className="col-md-4 col-4 main-content">
            <div className="col-md-12 comandas">
              {
                Object.keys(this.state.productosComanda).length !== 0 ?
                  this.state.productosComanda.map((items, index) => <RenderItems
                    key={index}
                    editarExtrasEnComanda={this.editarExtrasEnComanda}
                    handleDetalleItem={this.handleDetalleItem}
                    isLoading={isloading}
                    masUnElemento={this.masUnElementoHandler}
                    menosUnItem={this.menosUnElementoHandler}
                    borrarTodoElementoRow={this.borrarElementoRowHandler}
                    item={items} />)
                  :
                  <NoPedidos></NoPedidos>
              }
            </div>
            <Total
              descuento={this.state.descuento}
              subtotal={this.state.subtotal}
              total={this.state.total}
              agregarDescuento={this.agregarDescuento}
              guardarPedido={this.guardarPedido}
              pagarComanda={this.pagarComanda}
              nombrepedido={nombre_pedido}
              isLoading={isloading}
            />
          </div>
          <div className="col-md-8">
            {this.state.views.productos ?
              <Animated animationIn="bounceInRight" animationOut="fadeOut" isVisible={this.state.views.productos} >
                <div className="row">
                  <CategoriasTabs getParentData={this.onchangeGetDataCategoria}></CategoriasTabs>
                </div>
                {
                  !this.state.isLoading ?
                    <div className="row">
                      {Object.keys(this.state.productos).length !== 0 ?
                        this.state.productos.map((item, key) =>
                          <Productos AddProductsHandler={this.onclickAgregaProductos}
                            handleAgregarAcomandaNuevo={this.handleAgregarAcomandaNuevo}
                            items={item}
                            key={key} />)

                        : <NoProducto></NoProducto>

                      }
                    </div> :
                    <LoadingComponent size="lg" />
                }
              </Animated>
              : null
            }
            {this.state.views.extras ?
              <Animated animationIn="bounceInRight" animationOut="fadeOut" isVisible={this.state.views.extras}>
                <ExtrasProductos
                  isVisibleExtras={this.state.views.extras}
                  item_id={this.state.selectedItemId}
                  closeView={this.closeExtras}
                  handleExtra={this.handleExtra}
                  itemSelected={this.state.productoSeleccionadoTemp}
                  checkAvalibleExtras={this.state.validateExtrasObject} />
              </Animated>
              : null
            }

            {calculadora}

            {this.state.views.showItemDetail ?
              <EditarxItem
                modificarItemPorcentaje={this.modificarItemPorcentaje}
                closeViewPorcentaje={this.closeViewPorcentaje}
                item={this.state.productoSeleccionadoTemp} />
              : null
            }

            {this.state.views.showViewDescuento ?
              <AgregarDescuento
                total={this.state.total}
                subtotal={this.state.subtotal}
                closeViewPorcentaje={this.closeViewPorcentaje}
                calculaTotalDescuento={this.calculaTotalDescuento}
              />
              : null
            }

            {this.state.views.total_pagar ?
              <ShowTotalPago
                procesar={this.procesar}
                isVisible={this.state.views.total_pagar}
                total={this.state.total}
                show={this.showCalculadora}
                totalCambio={this.state.totalCambio} />
              : null}
          </div>
        </div>
        <AlertComponent type={this.state.modal.type}
          show={this.state.showmodal}
          title={this.state.modal.title}
          text={this.state.modal.text}
          closeModal={() => this.setState({ showmodal: false, isLoading: false })}
          confirmEvent={(inputValue) => this.confirmModal(inputValue)} />
      </div>
    )
  }
}
