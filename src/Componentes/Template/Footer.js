import React, { Component } from 'react';

class Footer extends Component {

    render() {
        return (
            <footer className="sticky-footer bg-white">
                <div className="container my-auto">
                    <div className="copyright text-center my-auto">
                        <span>Copyright © Chantico Qsr 2019 - </span> 
                        <b>Version: 1.0 </b>
                        <div>
                            <br/> 
                            <span>React version </span> 
                            <b>{React.version} </b>    
                        </div>
                    </div>
                    
                </div>
            </footer>
        );
    }
}

export default Footer;