import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as GLOBAL from '../Core/Constantes'
import Aux from '../Layout/AuxComponent'
class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collapsed: true,
    };
  }

  render() {
    let rol = this.props.role;
    return (
      <Aux>
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">
          <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/dashboard">
            <div className="sidebar-brand-icon rotate-n-15">
              <FontAwesomeIcon icon="coffee"></FontAwesomeIcon>
            </div>
            <div className="sidebar-brand-text mx-3">Chantico</div>
          </Link>
          {
            rol === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
              <li className="nav-item">
                <Link className="nav-link" to="/mesas">
                  <span> <FontAwesomeIcon size="lg" icon="list" /></span>
                </Link>
              </li>
              : null
          }
          {
            rol === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
              <li className="nav-item">
                <Link className="nav-link" to="/cerrarcaja">
                  <span> <FontAwesomeIcon size="lg" icon="money-bill-alt" /></span>
                </Link>
              </li>
              : null
          }

          {
            rol === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
              <li className="nav-item">
                <Link className="nav-link" to="/comandas/terminadas">
                  <span> <FontAwesomeIcon size="lg" icon="shopping-cart" /></span>
                </Link>
              </li>
              : null
          }

          {
            rol === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
              <div>
                <hr className="sidebar-divider"></hr>
                <div className="sidebar-heading">Admin</div>
                <li className="nav-item">
                  <Link className={(this.state.collapsed) ? "nav-link collapsed" : "nav-link"}
                    onClick={() => { this.setState({ collapsed: false }) }}
                    data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true"
                    aria-controls="collapseTwo" to="">
                    <FontAwesomeIcon icon="wrench" />
                  </Link>
                  <div id="collapseTwo" className={(this.state.collapsed) ? "collapse" : "collapse show"} aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div className="bg-white py-2 collapse-inner rounded">
                      <h6 className="collapse-header">Catalogos:</h6>
                      <Link onClick={() => { this.setState({ collapsed: true }) }} className="collapse-item" to="/admin/catalogo/categorias">Categorias</Link>
                      <Link onClick={() => { this.setState({ collapsed: true }) }} className="collapse-item" to="/admin/catalogo/estatus">Estatus</Link>
                      <Link onClick={() => { this.setState({ collapsed: true }) }} className="collapse-item" to="/admin/catalogo/productos">Productos</Link>
                      <Link onClick={() => { this.setState({ collapsed: true }) }} className="collapse-item" to="/admin/complementos">Complementos</Link>

                    </div>
                  </div>
                </li>
                <hr className="sidebar-divider d-none d-md-block"></hr>
              </div>
              : null
          }
        </ul>
      </Aux>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token !== null,
    role: parseInt(state.auth.role),
    cerrada: state.auth.cajaCerrada,
  }
}

export default connect(mapStateToProps)(Sidebar);