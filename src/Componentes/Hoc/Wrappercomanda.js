import React, { Component } from 'react'

export default (WrappedComponent) => {
    class Wrappercomanda extends Component {
        constructor(props) {
            super(props);
            this.state = {
                total: 0,
                descuento: 0,
                subtotal: 0,
                productos: [],
            }
        }

        render() {
            let { total, subtotal } = this.state;
            return <WrappedComponent total={total} subtotal={subtotal}></WrappedComponent>
        }
    }

    return Wrappercomanda
}
