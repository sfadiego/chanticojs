import * as ActionTypes from '../actions/ActionTypes'
import { updateObject } from '../../utility'

const initialState = {
    token: null,
    userId: null,
    userFirstName: null,
    userLastName: null,
    username: null,
    role: null,
    error: null,
    tienda: null,
    cajaCerrada: null,
    cerradaHasta:null,
    loading: false
};

const authStart = (state, action) => updateObject(state, { error: null, loading: true });

const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.token,
        userId: action.userId,
        userFirstName: action.userFirstName,
        userLastName: action.userLastName,
        username: action.username,
        role: action.role,
        tienda: action.tienda,
        cajaCerrada: action.cajaCerrada,
        cerradaHasta: action.cerradaHasta,
        error: null,
        loading: false
    });
};

const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}

const authLogout = (state, action) => {
    return updateObject(state, {
        token: null,
        userId: null,
        userFirstName: null,
        userLastName: null,
        username: null,
        tienda: null,
        cajaCerrada: null,
        cerradaHasta:null,
        rol: null,
    })
}

const closeSales = (state, action) => {
    return updateObject(state, {
        cajaCerrada: action.cajaCerrada,
        loading:action.loading
    });
}

const openSales = (state, action) => {
    return updateObject(state, {
        cajaCerrada: action.cajaCerrada,
        loading:action.loading
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.AUTH_START: return authStart(state, action);
        case ActionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case ActionTypes.AUTH_FAIL: return authFail(state, action);
        case ActionTypes.AUTH_LOGOUT: return authLogout(state, action);
        case ActionTypes.CLOSE_SALES: return closeSales(state, action);
        case ActionTypes.OPEN_SALES: return openSales(state, action);
        default: return state;
    }
}

export default reducer;