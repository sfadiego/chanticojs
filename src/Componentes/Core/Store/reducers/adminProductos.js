import * as ActionTypes from '../actions/ActionTypes';
import { updateObject } from '../../utility';

const initialState = {
    producto_id: null,
}

const clearProduct = (state, action) => updateObject(state, { producto_id: null});
const saveProduct = (state, action) => {
    return updateObject(state, {
        producto_id: action.producto_id,
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.SAVE_ITEM_PRODUCT_INFO: return saveProduct(state, action)
        case ActionTypes.CLEAR_ITEM_PRODUCT_INFO: return clearProduct(state, action)
        case ActionTypes.SET_PRODUCT_ID: return saveProduct(state, action)
        default: return state;
    }
}

export default reducer;