const LocalStorageService = (function () {
    function _setToken(token, data) {
        const expirationDate = new Date(new Date().getTime() + 60 * 1000 * 8)
        localStorage.setItem('token', token);
        localStorage.setItem('expirationDate', expirationDate)
        localStorage.setItem('userId', data._id)
        localStorage.setItem('nombre', data.nombre)
        localStorage.setItem('apellido', data.apellido)
        localStorage.setItem('username', data.username)
        localStorage.setItem('role', data.role)
        localStorage.setItem('tienda', data.tienda.tienda)
        localStorage.setItem('cajaCerrada',  data.tienda.cajaCerrada)
        localStorage.setItem('cerradaHasta',  data.tienda.cerradaHasta)
    }
    function _getAccessToken() {
        return localStorage.getItem('token');
    }

    function _getLocalStorage() {
        return {
            token: localStorage.getItem('token'),
            expirationDate: localStorage.getItem('expirationDate'),
            userId: localStorage.getItem('userId'),
            nombre: localStorage.getItem('nombre'),
            apellido: localStorage.getItem('apellido'),
            username: localStorage.getItem('username'),
            role: localStorage.getItem('role'),
            tienda: localStorage.getItem('tienda'),
            cajaCerrada: localStorage.getItem('cajaCerrada'),
            cerradaHasta: localStorage.getItem('cerradaHasta'),
        }
    }

    function _clearToken() {
        localStorage.removeItem('token');
        localStorage.removeItem('expirationDate')
        localStorage.removeItem('userId')
        localStorage.removeItem('nombre')
        localStorage.removeItem('apellido')
        localStorage.removeItem('username')
        localStorage.removeItem('role')
        localStorage.removeItem('tienda')
        localStorage.removeItem('cajaCerrada')
        localStorage.removeItem('cerradaHasta')
    }

    function _cerrarVentas(){
        localStorage.removeItem('cajaCerrada');
        localStorage.setItem('cajaCerrada',  true)
    }

    function _abrirVentas(){
        localStorage.removeItem('cajaCerrada');
        localStorage.setItem('cajaCerrada',  false)
    }

    return {
        setToken: _setToken,
        getAccessToken: _getAccessToken,
        clearToken: _clearToken,
        cerrarVentas:_cerrarVentas,
        abrirVentas:_abrirVentas,
        getLocalStorage: _getLocalStorage
    }
})();
export default LocalStorageService;