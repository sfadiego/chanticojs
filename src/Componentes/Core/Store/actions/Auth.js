import * as ActionTypes from './ActionTypes'
import Axios from 'axios'
import LocalStorageService from '../localstorage/LocalStorageService'

export const authStart = () => {
    return {
        type: ActionTypes.AUTH_START
    }
}

export const authSuccess = (token, userdata) => {
    let cerrado = userdata.cajaCerrada === "true" ? true : false;
    let userId = userdata._id,
        userFirstName = userdata.nombre,
        userLastName = userdata.apellido,
        username = userdata.username,
        role = userdata.role,
        tienda = userdata.tienda.tienda ? userdata.tienda.tienda : userdata.tienda,
        cajaCerrada = userdata.tienda.cajaCerrada ? userdata.tienda.cajaCerrada : cerrado,
        cerradaHasta = userdata.tienda.cerradaHasta ? userdata.tienda.cerradaHasta : userdata.cerradaHasta
    return {
        type: ActionTypes.AUTH_SUCCESS,
        token: token,
        userId,
        userFirstName,
        userLastName,
        username,
        role,
        tienda,
        cajaCerrada,
        cerradaHasta,
        error: null,
        loading: false
    }
}

export const authFail = (error) => {
    return {
        type: ActionTypes.AUTH_FAIL,
        error: error
    }
}

export const sessionTimeout = (expirationDate) => {
    return dispatch => {
        setTimeout(() => dispatch(authLogOut()), expirationDate);
    }
}

export const authLogOut = () => {
    LocalStorageService.clearToken();
    return {
        type: ActionTypes.AUTH_LOGOUT,
    }
}

export const auth = (usuario, password) => {
    return dispatch => {
        dispatch(authStart());
        let authData = {
            username: usuario,
            password: password,
            returnSecureToken: true
        }
        Axios.post('login', authData)
            .then(response => {
                let data = response.data;
                let token = data.token;
                let userdata = data.data;
                LocalStorageService.setToken(token, userdata);
                dispatch(authSuccess(token, userdata))
                dispatch(sessionTimeout(60 * 1000 * 8))
            }).catch(error => dispatch(authFail(error.response)));
    }

}

export const authCheckState = () => {
    return dispatch => {
        const token = LocalStorageService.getAccessToken();
        if (!token) {
            return dispatch(authLogOut())
        } else {
            const storage = LocalStorageService.getLocalStorage()
            const expirationDate = new Date(storage.expirationDate);
            if (expirationDate <= new Date()) {
                return dispatch(authLogOut())
            } else {
                dispatch(authSuccess(token, storage))
                let expiration = 60 * 10000 * 8;
                dispatch(sessionTimeout(expiration));
            }
        }
    }
}

export const cerrar = () => {
    LocalStorageService.cerrarVentas()
    return {
        type: ActionTypes.CLOSE_SALES,
        cajaCerrada: true,
        loading: false
    }
}

export const closeFail = (error) => {
    return {
        type: ActionTypes.CLOSE_SALES_FAIL,
        cajaCerrada: false,
        error: error
    }
}

export const abrir = () => {
    LocalStorageService.abrirVentas()
    return {
        type: ActionTypes.OPEN_SALES,
        cajaCerrada: false,
        loading: false
    }
}

export const openSalesFail = (error) => {
    return {
        type: ActionTypes.CLOSE_SALES_FAIL,
        cajaCerrada: true,
        error: error
    }
}

export const closeSales = (tiendaNombre) => {
    return dispatch => {
        Axios.post('/cerrarcaja', { tienda: tiendaNombre })
            .then(response => dispatch(cerrar()))
            .catch(error => dispatch(closeFail(error)))
    }
}

export const openSales = (tienda) => {
    return dispatch => {
        Axios.post('/abrircaja', { tienda: tienda })
            .then(response => dispatch(abrir()))
            .catch(error => dispatch(openSalesFail(error)))
    }
}