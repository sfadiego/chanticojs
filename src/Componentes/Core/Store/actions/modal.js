import * as ActionTypes from './ActionTypes';

const setModalInfo = (data) => {
    return {
        type:ActionTypes.MODAL_SHOW,
        title: data.title,
        modaltype: data.type,
        text: data.text,
    }
}

export const showModal = (data) => {
    return dispatch => dispatch(setModalInfo(data))
}