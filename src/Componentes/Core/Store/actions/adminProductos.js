import * as ActionTypes from './ActionTypes'

export const saveTemProductInfo = (producto_id) => {
    return {
        type: ActionTypes.SAVE_ITEM_PRODUCT_INFO,
        producto_id: producto_id,
    }
}

export const clearTemProductInfo = () => {
    return {
        type: ActionTypes.CLEAR_ITEM_PRODUCT_INFO,
        producto_id:null,
    }
}

export const setproductId = (producto_id)=>{
    return {
        type: ActionTypes.SET_PRODUCT_ID,
        producto_id:producto_id
    }
}

export const saveProduct = (id) => {
    return dispatch => dispatch(saveTemProductInfo(id))
}

export const clearProduct = () => {
    return dispatch => dispatch(clearTemProductInfo())
}

export const setId = (id) => {
    return dispatch => dispatch(setproductId(id))
}