import React, { Component } from 'react'
import { Animated } from 'react-animated-css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DisplayToolbar from './DisplayToolbar';
import Buttons from './Buttons';
import { Button } from 'react-bootstrap';
import * as Calculator from './calculator-core'
import { Link } from 'react-router-dom';

let GLOBAL = require("../../Core/Constantes");

export default class Calculadora extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formula: [],
      input: '0',
      afterCalculation: false,
      isVisible: props.isVisible || false,
      printTicket: false
    }
    this.onClear = this.onClear.bind(this);
    this.onEqual = this.onEqual.bind(this);
    this.onDigit = this.onDigit.bind(this);
    this.onOperator = this.onOperator.bind(this);
    this.onDecimal = this.onDecimal.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.total !== this.props.total) {
      this.setState({
        formula: [].concat(this.props.total)
      });
    }
  }

  componentDidMount() {
    this.setState({
      formula:[].concat(this.props.total),
      input: "-",
      afterCalculation: false,
    })
  }

  onClear() {
    let formula = [this.props.total]
    this.setState({
      formula: formula,
      input: '-',
      afterCalculation: false
    });
  }
  onEqual() {
    const finalFormula = this.state.formula.concat(this.state.input);
    const result = Calculator.evaluate(finalFormula);
    if (result > 0) { return this.props.handleModal(GLOBAL.INVALID_CASH_AMOUNT, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, false); }

    if (!Number.isNaN(result)) {
      this.setState({
        input: result + "",
        totalCambio: result,
        formula: [],
        afterCalculation: true,
      });

      return this.props.onEqual(result);
    }

  }
  onDigit({ target }) {
    const digit = target.innerText;
    const input = this.state.input;

    if (this.state.afterCalculation) {
      this.setState({
        input: digit,
        afterCalculation: false
      });
    } else if (input === '0') {
      this.setState({
        input: digit
      });
    } else if (Calculator.isNotNumber(input)) {
      this.setState({
        input: digit,
        formula: this.state.formula.concat(input)
      });
    } else {
      this.setState({
        input: input.concat(digit)
      });
    }
  }
  onOperator({ target }) {
    const operator = target.innerText;
    const input = this.state.input;
    if (Calculator.isOperator(input)) {
      this.setState({
        input: operator,
        afterCalculation: false
      });
    } else if (input !== '(') {
      this.setState({
        formula: this.state.formula.concat(this.state.input),
        input: operator,
        afterCalculation: false
      });
    }
  }
  onDecimal({ target }) {
    const decimal = target.innerText;
    const input = this.state.input;

    if (this.state.afterCalculation) {
      this.setState({
        input: `0${decimal}`,
        afterCalculation: false
      });
    } else if (Calculator.isNotNumber(input)) {
      this.setState({
        input: `0${decimal}`,
        formula: this.state.formula.concat(input)
      });
    } else if (!input.includes(decimal)) {
      this.setState({
        input: input.concat(decimal),
      });
    }
  }


  render() {
    return (
      <Animated animationIn="bounceInRight" animationOut="fadeOut" isVisible={this.state.isVisible}>
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-5">
            <h4 className="color444444"> Deuda: <span className="text-danger"> ${this.props.total}  </span></h4>
            <DisplayToolbar
              formula={this.state.formula}
              input={this.state.input}
            />
            <Buttons
              onClear={this.onClear}
              onEqual={this.onEqual}
              onDecimal={this.onDecimal}
              onDigit={this.onDigit}
              onOperator={this.onOperator}
            />

            <Button disabled={(this.state.formula.length === 2) ? false : true} className="btn btn-primary col-md-6 br-0" onClick={this.onEqual}>
              <FontAwesomeIcon icon="hand-holding-usd"></FontAwesomeIcon> Pagar
                      </Button>
            {
              <Link className="btn btn-success col-md-6 br-0" to={`/imprimir/ticket/${this.props.comandaid}`} onClick={() => this.setState({ printTicket: true })}>
                <FontAwesomeIcon icon="clipboard"></FontAwesomeIcon> Generar Ticket
                      </Link>
            }

          </div>
          <div className="col-md-3"></div>
        </div>
      </Animated>
    )
  }
}
