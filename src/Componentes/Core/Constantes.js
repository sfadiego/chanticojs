const PATH_URL = "http://localhost:3001/"
const MODAL_SUCCESS = "success"
const STORE_NAME = "Chantico";
const MODAL_ERROR = "error";
const DEFAULT_MODAL_TITLE = "Chantico";
const FILE_REQUIRED = "Imagen de producto obligatoria"
const SUCCES_MESSAGE = "Se creo el registro exitosamente";
const SUCCES_DELETE = "Se borro el registro exitosamente";
const SUCCES = "Transacción Exitosa";
const SUCCES_WITHOUT_FILE = "Transacción Exitosa sin archivo";

const INVALID_FILE_TYPE = "Tipo de archivo no permitido";
const ERROR_SAVING = "Ha ocurrido un error creando registro";
const ERROR_UPDATING = "Ha ocurrido un error actualizando registro";
const ERROR_DELETING = "Ha ocurrido un error borrando registro";
const ERROR_RETRIEVING_DATA = "Error obteniendo información";
const UNAVAILABLE_CONTENT = "Contenido no disponible";
const ITEMS_REQUIRED = "Agregar elementos al pedido";
const REQUIRED_FIELD = "Campo requerido";
const INVALID_CASH_AMOUNT = "Cantidad ingresada menor que el Total";
const UNIQUE_USERNAME = 'El usuario ingresado ya existe';

const ADD_DISCOUNT = "Agregar descuento?";
const ORDER_CREATED = "Pedido creado";
const ORDER_UPDATED = "Pedido actualizado";
const SELECT_ITEM = "Selecciona un elemento";
const FORM_INPUTS_REQUIRED = "Ingresa los campos del formulario";
const CATEGORIA_ID = "categoria_id";
const CATEGORIA = "categoria";
const CANTIDAD = "cantidad";
const PRODUCTO = "producto";
const PRECIO = "precio";
const PRODUCTO_ID = "producto_id";
const ESTATUS_ID = "estatus_id";
const ESTATUS = "estatus";
const COLOR = "color";
const ICON = "icon";
const CSSCLASS = "cssclass";
const UNIQUE = "unique";
const PEDIDO_ID = "pedido_id";
const TOTAL = "total";
const NOMBRE_PRODUCTO = "nombre_producto";
const PRODUCTO_ABREVIADO = "producto_abreviado";
const PRECIO_PROVEEDOR = "precio_proveedor";
const DESCRIPCION = "descripcion";
const PRECIO_VENTA = "precio_venta";
const INVENTARIO = "inventario";
const EXISTENCIA = "existencia";
const ID_COLOR = "id_color";
const INVENTARIADO = "inventariado";
const URL_IMAGEN = "url_imagen";
const DETALLE_PRODUCTO_ID = "detalle_producto_id"
const FOLDER_NAME = 'nombre_carpeta';
const CONFIRMBUTTONCOLOR = "#3085d6"
const TYPE_INPUT = "input";
const MAX_VALUE_EXCEED = "Se exedio la máxima cantidad";
const POST = "POST";
const PUT = "PUT";
const DELETE = "DELETE";
const GET = "GET";

// Error Messages
const API_CANCELADA = "[E001_CT] CANCELTOKEN";
const API_CANCEL_REQUEST = "[E002_CR] CANCELTOKEN ON REQUEST";
// tablas 
const BEBIDAS_ID = 1;
const SABOR_ID = 2;
const ADEREZO_ID = 3;
const PROTEINA_ID = 4;
const SABOR_DULCE_ID = 5

const CAT_ESTATUS = {
    OCUPADO: "ocupado",
    DISPONIBLE: "disponible",
    FINALIZADO: "finalizado",
    PROCESO: "proceso",
};

const CAT_ROLES = {
    ADMINISTRADOR: 1,
    USUARIO: 2
}

const VIEWS = {
    CALCULADORA: 1,
    PRODUCTOS: 2,
    TOTAL_PAGOS: 3,
    EXTRAS: 4,
    READONLY: 5,
    DESCUENTO: 6,
    DESCUENTO_TOTAL: 7
}

module.exports = {
    TABLA_EXTRAS: {
        BEBIDAS_ID,
        SABOR_ID,
        SABOR_DULCE_ID,
        ADEREZO_ID,
        PROTEINA_ID
    },
    CAT_ROLES,
    VIEWS,
    CAT_ESTATUS,
    INVALID_CASH_AMOUNT,
    API_CANCEL_REQUEST,
    ORDER_UPDATED,
    SUCCES_WITHOUT_FILE,
    STORE_NAME,
    MAX_VALUE_EXCEED,
    FORM_INPUTS_REQUIRED,
    TYPE_INPUT,
    REQUIRED_FIELD,
    SELECT_ITEM,
    ADD_DISCOUNT,
    API_CANCELADA,
    PATH_URL,
    CONFIRMBUTTONCOLOR,
    MODAL_ERROR,
    DEFAULT_MODAL_TITLE,
    MODAL_SUCCESS,
    FILE_REQUIRED,
    SUCCES_MESSAGE,
    SUCCES_DELETE,
    SUCCES,
    INVALID_FILE_TYPE,
    ERROR_SAVING,
    ERROR_RETRIEVING_DATA,
    UNAVAILABLE_CONTENT,
    ERROR_UPDATING,
    ERROR_DELETING,
    CATEGORIA_ID,
    CATEGORIA,
    CANTIDAD,
    PRODUCTO,
    PRECIO,
    PRODUCTO_ID,
    ESTATUS_ID,
    ESTATUS,
    COLOR,
    ICON,
    CSSCLASS,
    UNIQUE,
    PEDIDO_ID,
    TOTAL,
    UNIQUE_USERNAME,
    NOMBRE_PRODUCTO,
    PRODUCTO_ABREVIADO,
    PRECIO_PROVEEDOR,
    DESCRIPCION,
    PRECIO_VENTA,
    INVENTARIO,
    EXISTENCIA,
    ID_COLOR,
    INVENTARIADO,
    URL_IMAGEN,
    DETALLE_PRODUCTO_ID,
    FOLDER_NAME,
    ITEMS_REQUIRED,
    ORDER_CREATED,
    POST,
    PUT,
    DELETE,
    GET
}