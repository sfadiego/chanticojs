import axios from 'axios';
let GLOBAL = require("../Core/Constantes");
export function formatDate(dateString = null) {
  var monthNames = [
    "Enero", "Febrero", "Marzo",
    "Abril", "Mayo", "Junio", "Julio",
    "Agosto", "Septiembre", "Octubre",
    "Noviembre", "Diciembre"
  ];


  let date = (dateString) ? new Date(dateString) : new Date();
  var dia = date.getDate();
  var mesIndex = date.getMonth();
  var year = date.getFullYear();

  return dia + ' ' + monthNames[mesIndex] + ' ' + year;
}

export function getDataAxios(endpoint, token) {
  return new Promise((resolve, reject) => {
    axios.get(`${endpoint}`, { cancelToken: token })
      .then((result) => resolve(result.data))
      .catch((err) => reject(err));
  })
}

export function requestAxios(endpoint, method, data) {
  return new Promise((resolve, reject) => {
    axios({
      method: method,
      url: `${endpoint}`,
      data: data
    })
      .then((result) => resolve(result.data))
      .catch((err) => reject(err));
  })
}

export function getStatusProceso(data) {
  let filtro = data.find(item => item.estatus === "proceso");
  return filtro
}

export function getStatusFinalizado(data) {
  let filtro = data.find(item => item.estatus === "finalizado");
  return filtro
}

export function getStatusOcupado(data) {
  let filtro = data.find(item => item.estatus === "ocupado");
  return filtro
}

export function calculaExtras(array) {
  return new Promise((resolve, reject) => {
    let extras = array;
    let totales = 0
    if (extras && extras.length > 0) {
      extras.map(item => {
        if (!item.precio) {
          totales = 0;
          return false
        }
        totales = totales + item.precio
        return totales
      })
      return resolve(totales)
    }

    return reject(totales);
  });
}

export function calculaExtrasYproductoxRow(data) {
  let totales = 0
  let extras = data.extras ? data.extras : [];
  let producto = data;
  if (extras.length > 0) {
    extras.map(item => {
      if (!item.precio) {
        totales = 0;
        return false
      }
      totales = totales + item.precio
      return totales
    })
  }

  return producto.cantidad * (producto.precio + totales)
}

export function calculaTotalExtrasxRow(data) {
  let totales = 0
  let extras = data.extras ? data.extras : [];
  if (extras.length > 0) {
    extras.map(item => {
      if (!item.precio) {
        totales = 0;
        return false
      }
      totales = totales + item.precio
      return totales
    })
  }
  return totales;
}

export function calculaTotal(item, total_actual, extras) {
  let cantidad = item.cantidad
  let precio = item.precio
  let descuento = item.descuento;

  let total = total_actual
  let calculaTotal = 0
  calculaTotal = cantidad * (precio + extras);

  if (descuento > 0) {
    let precioDescuento = this.calculaTotalConDescuento(calculaTotal, descuento)
    return { total: precioDescuento + total, subtotal: calculaTotal + total }
  }

  let finalTotal = calculaTotal + total
  return { total: finalTotal, subtotal: finalTotal }
}

export function calculaTotalConDescuento(total, descuento) {
  var porcentaje = parseInt(descuento);
  var ActualTotal = total;
  if (descuento === 0) {
    return total;
  }
  let Total_descuento = (ActualTotal / 100) * porcentaje
  return Math.round(ActualTotal - Total_descuento)
}

export function generaFolioPedido() {
  var date = new Date();
  let key = date.getMilliseconds();
  return `CHANT-${key}-${date.getDay()}-${date.getMonth()}`
}


export function pagarComanda(objComanda) {
  return new Promise((resolve, reject) => {
    axios({
      method: GLOBAL.POST,
      url: 'comandapedido/multiple',
      data: objComanda
    }).then(response => resolve(response))
      .catch(error => reject(error));
  })

}

export function obtenerExtrasTicket(item) {
  let extras = [];
  if (item.extras.length > 0) {
    item.extras.map((elemento) => {
      if (elemento.precio > 0) {
        return extras.push({ nombre: elemento.nombre, precio: elemento.precio })
      } else {
        return false
      }
    })
  }

  return extras;
}