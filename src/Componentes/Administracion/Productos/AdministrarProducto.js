import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../Core/Store/actions/index';

import axios from 'axios';
import AlertComponent from '../../AlertComponents/AlertComponent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CardAderezos from '../../Elementos/CardAderezos';
import CardSabores from '../../Elementos/CardSabores';
import CardProteinas from '../../Elementos/CardProteinas';
import CardTipoBebida from '../../Elementos/CardTipoBebida';
import CardSaborDulce from '../../Elementos/CardSaborDulce';
import LoadingComponent from '../../Elementos/LoadingComponent'

let GLOBAL = require("../../Core/Constantes");

class AdministrarProducto extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.state = {
            showmodal: false,
            modal: {
                title: '',
                icon: GLOBAL.MODAL_SUCCESS,
                type: GLOBAL.MODAL_SUCCESS,
                text: ""
            },
            producto: "",
            precio: null,
            activo: true,
            categoria_id: '',
            cantidad: "",
            productos: [],
            categorias: [],
            file: null,
            img_url: null,
            producto_id: null,
            msg_error: {},
            request_code: null,
            isLoading: true
        };

        this.handleSubmitevent = this.handleSubmitevent.bind(this);
    }

    signal = axios.CancelToken.source();

    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {

        if (this.props.match.params.id) {
            this.cargarProductoInfo(this.props.match.params.id);
        }

        if (!this.state.img_url && !this.state.file) {
            this.setState({ img_url: `${GLOBAL.PATH_URL}noimagen` })
        }

        this.loadComponentInfo();
    }

    loadComponentInfo = async () => {
        try {

            let objCategoria = await axios.get('categoria', { cancelToken: this.signal.token })
            this.setState({ categorias: objCategoria.data, isLoading: false });

        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            }
        }
    }

    handleModal(text = GLOBAL.SUCCES, type = GLOBAL.MODAL_SUCCESS, title = GLOBAL.MODAL_SUCCESS, showmodal = true, request_code = null) {
        let dataOptions = {
            ...this.state.modal,
            text: text,
            type: type,
            title: title
        };

        return this.setState({
            modal: dataOptions,
            showmodal: showmodal,
            status_code: request_code,
        });
    }

    cargarProductoInfo = async (idProducto) => {

        try {
            let id = idProducto
            let catProductos = await axios.get(`catproductos/${id}`, {
                cancelToken: this.signal.token
            });
            let data_productos = catProductos.data;
            let producto_id = data_productos._id;
            let producto = data_productos.producto;
            let precio = data_productos.precio;
            let cantidad = data_productos.cantidad;
            let categoria = data_productos.categoria_id[0]._id;
            this.props.setproductId(producto_id);
            this.setState({
                producto: producto,
                cantidad: cantidad,
                precio: parseInt(precio),
                producto_id: producto_id,
                categoria_id: categoria,
                isLoading: false
            });
            let item_id = this.props.match.params.id
            let cargarimgData = await axios.get(`fileinfoByIdItem/${item_id}`, { cancelToken: this.signal.token })
            let img_data = cargarimgData.data
            let url_img = img_data.length > 0 ? `${GLOBAL.PATH_URL}${img_data[0].public_path}` : `${GLOBAL.PATH_URL}noimagen`;

            this.setState({ img_url: url_img })


        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            } else { this.setState({ img_url: `noimagen` }); }

        }

    }



    onReturn = () => {
        this.setState({ showmodal: false });
        let path = `/admin/catalogo/productos/`;
        this.props.history.push(path);
        return this.props.onclear()
    }

    validarFormulario() {
        let status = true;
        if (this.state.file === null) {
            this.handleModal(GLOBAL.FILE_REQUIRED, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400);
            status = false;
        }

        return status;
    }

    handleFile(e) {
        let file = e.target.files[0];
        this.setState({
            file: file
        });
    }

    handleFileUpload = async () => {
        try {
            let file = this.state.file;
            let formData = new FormData();
            formData.append('archivo', file);
            formData.append('item_id', this.props.producto_id);
            let upload = await axios({
                url: 'uploadFile/productos/',
                method: GLOBAL.PUT,
                data: formData
            })

            return upload;

        } catch (error) {
            this.handleModal(GLOBAL.ERROR_SAVING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400);
        }
    }

    submitEvent = async () => {
        try {
            if (!this.state.categoria_id || !this.state.precio || !this.state.cantidad || !this.state.producto) {
                this.handleModal(GLOBAL.FORM_INPUTS_REQUIRED, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400);
            }

            let endpoints = (this.props.producto_id) ? `catproductos/${this.props.producto_id}` : 'catproductos'
            axios({
                method: (this.props.producto_id) ? GLOBAL.PUT : GLOBAL.POST,
                url: `${endpoints}`,
                data: {
                    categoria_id: this.state.categoria_id,
                    precio: this.state.precio,
                    activo: this.state.activo,
                    cantidad: this.state.cantidad,
                    producto: this.state.producto
                },
            }, { cancelToken: this.signal.token })
                .then(response => {
                    let response_producto = response.data;
                    if (response_producto._id && this.state.file !== null) {
                        this.setState({ producto_id: response_producto._id })
                        this.handleFileUpload();
                        this.handleModal(GLOBAL.SUCCES, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200);
                        return this.props.onSaveProduct(response_producto.data._id)
                    }
                    else {
                        this.handleModal(GLOBAL.SUCCES, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200);
                        return this.props.onSaveProduct(response_producto.data._id)
                    }

                })
                .catch(error => this.handleModal(GLOBAL.SUCCES, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200))


        } catch (error) {
            this.handleModal(GLOBAL.ERROR_SAVING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 500);
        }
    }

    handleSubmitevent(event) {
        event.preventDefault();
        this.submitEvent();
    }

    render() {
        let categorias = this.state.categorias;
        let img = (!this.state.file) ? this.state.img_url : URL.createObjectURL(this.state.file);
        let mensaje = <AlertComponent type={this.state.modal.type}
            show={this.state.showmodal}
            title={(this.state.modal.title) ? this.state.modal.title : ''}
            text={(this.state.modal.text) ? this.state.modal.text : ''}
            closeModal={() => {
                this.setState({
                    showmodal: false
                });
            }}
            confirmEvent={() => {
                if (this.state.status_code >= 400) {
                    return this.setState({ showmodal: false });
                } else if (this.props.producto_id) {
                    return this.setState({ showmodal: false });
                }
            }} ></AlertComponent>

        let form = <form onSubmit={this.handleSubmitevent}>
            <div className="form-group">
                <label htmlFor="producto">Nombre del producto <span style={{ color: "red" }}>*</span></label>
                <input type="text" placeholder="Ejemplo: Café" className="form-control" value={this.state.producto} name="producto" id="producto" onChange={event => this.setState({ producto: event.target.value })}></input>
                <span style={{ color: "red" }}>{this.state.msg_error['producto']}</span>
            </div>
            <div className="form-group">
                <label htmlFor="color">Precio <span style={{ color: "red" }}>*</span></label>
                <input type="number" min="0" placeholder="$" className="form-control" name="precio" value={(this.state.precio != null) ? this.state.precio : ''} id="precio" onChange={event => this.setState({ precio: event.target.value })}></input>
                <span style={{ color: "red" }}></span>
            </div>
            <div className="form-group">
                <label htmlFor="color">Cantidad || Descripción <span style={{ color: "red" }}>*</span></label>
                <input type="text" placeholder="500 ml" className="form-control" name="cantidad" value={this.state.cantidad} id="cantidad" onChange={event => this.setState({ cantidad: event.target.value })}></input>
                <span style={{ color: "red" }}></span>
            </div>
            <div className="form-group">
                <label htmlFor="icono">Categoria <span style={{ color: "red" }}>*</span></label>
                <select value={this.state.categoria_id} className="form-control" name="categoria" id="categoria" onChange={event => this.setState({ categoria_id: event.target.value })}>
                    <option> Selecciona una categoria </option>
                    {categorias.map((item, key) => {
                        return <option key={key} value={item._id}> {item.categoria} </option>
                    })}
                </select>
                <span style={{ color: "red" }}></span>
            </div>
            <div className="form-group">
                <label htmlFor="icono">Imagen de producto <span style={{ color: "red" }}>*</span></label>
                <input accept="image/*" className="form-control-file" onChange={(e) => { this.handleFile(e) }} type="file" />
                <span style={{ color: "red" }}></span>
            </div>
            <div className="form-group">
                <img src={img} alt="Imagen producto" className="img-thumbnail img-producto-thumbnail" ></img>
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-primary col-md-3">{!this.props.producto_id ? "Guardar" : "Actualizar"}</button>
            </div>
        </form>

        if (this.state.isLoading) {
            form = <LoadingComponent></LoadingComponent>
        }

        return (
            <div className="container-fluid" ref={this.myRef} >
                <div className="row">
                    <div className="col-md-12 text-gray-800">
                        <h1 className="h3 mb-0 text-gray-800">{this.state.producto ? this.state.producto : "Nuevo producto"}</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger btn-lg" onClick={() => { this.onReturn(); }}>
                            <FontAwesomeIcon icon="arrow-left"></FontAwesomeIcon> Regresar
                        </span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <ul className="nav nav-pills" id="productosTab" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" id="producto-tab" data-toggle="tab" href="#producto-content" role="tab" aria-controls="producto" aria-selected="true">Producto</a>
                            </li>

                            {
                                this.props.producto_id ?
                                    <li className="nav-item">
                                        <a className="nav-link" id="extras-tab" data-toggle="tab" href="#extras-content" role="tab" aria-controls="profile" aria-selected="false">Extras</a>
                                    </li> : null
                            }
                        </ul>
                        <div className="tab-content pt-3" id="productosContent">
                            <div className="tab-pane fade show active" id="producto-content" role="tabpanel" aria-labelledby="producto-tab">
                                {form}
                            </div>
                            <div className="tab-pane fade" id="extras-content" role="tabpanel" aria-labelledby="extras-tab">
                                <div className="form-group">
                                    <label htmlFor="Aderezos">Selecciona los aderezos para este producto</label>
                                    <CardAderezos producto_id={this.props.producto_id} > </CardAderezos>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="Sabores">Selecciona los Sabores para este producto</label>
                                    <CardSabores producto_id={this.props.producto_id} > </CardSabores>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="Proteinas">Selecciona las proteinas para este producto</label>
                                    <CardProteinas producto_id={this.props.producto_id} > </CardProteinas>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="Proteinsa">Selecciona los tipos de bebida para este producto</label>
                                    <CardTipoBebida producto_id={this.props.producto_id} > </CardTipoBebida>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="sabor dulce">Selecciona los tipos de sabores dulces para este producto</label>
                                    <CardSaborDulce producto_id={this.props.producto_id} > </CardSaborDulce>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {mensaje}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        producto_id: state.productos.producto_id
    }
}
const mapPropsToDispatch = (dispatch) => {
    return {
        onSaveProduct: (producto_id) => dispatch(actions.saveProduct(producto_id)),
        onclear: () => dispatch(actions.clearProduct()),
        setproductId: (producto_id) => dispatch(actions.setId(producto_id))
    }
}
export default connect(mapStateToProps, mapPropsToDispatch)(AdministrarProducto);