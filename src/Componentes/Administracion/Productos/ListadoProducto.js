import React, { Component } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import * as TablaProductos from './../../Tablas/TablaProductosConfig'
import AlertComponent from '../../AlertComponents/AlertComponent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap'
import LoadingComponent from '../../Elementos/LoadingComponent';
let GLOBAL = require("../../Core/Constantes");

class ListadoProducto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showmodal: false,
            modal: {
                title: '',
                icon: GLOBAL.MODAL_SUCCESS,
                type: GLOBAL.MODAL_SUCCESS,
                text: ""
            },
            columnas: [],
            productos: [],
            msg_error: {},
            isLoading: true
        }

        this.editElementHandler = this.editElementHandler.bind(this);
    }

    signal = axios.CancelToken.source();

    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        this.cargarListado();
    }

    cargarListado = async () => {
        try {
            let listado = await axios.get('catproductos', { cancelToken: this.signal.token });
            let productos = listado.data;
            let columnas = TablaProductos.columnsCatProductos();
            columnas.push({
                name: '--',
                sortable: true,
                center: true,
                cell: row => {
                    let id = `/admin/productos/${row._id}`
                    return <Link className="btn btn-warning" to={id}>  <FontAwesomeIcon icon="edit"></FontAwesomeIcon> </Link>
                }
            })
            columnas.push({
                name: '--',
                sortable: true,
                center: true,
                cell: row => {
                    return <Button className="btn btn-danger" onClick={() => {
                        axios.delete(`catproductos/${row._id}`)
                            .then(success => {
                                let dataOptions = {
                                    ...this.state.modal,
                                    type: GLOBAL.MODAL_ERROR,
                                    text: GLOBAL.SUCCES_DELETE,
                                }
                                this.setState({ showmodal: true, modal: dataOptions, msg_error: {} });
                            });
                    }}>  <FontAwesomeIcon icon="trash"></FontAwesomeIcon> </Button>
                }
            })
            this.setState({ showmodal: false, productos: productos, isLoading: false, columnas });
        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            }
        }
    }

    editElementHandler = data => {
        let path = `/admin/productos/${data}`
        this.props.history.push(path);
    }

    onclickAgrega = () => {
        let path = `/admin/productos/`
        this.props.history.push(path);
    }

    render() {
        let mensaje = <AlertComponent type={this.state.modal.type}
            show={this.state.showmodal}
            title={this.state.modal.title}
            text={this.state.modal.text}
            closeModal={() => {
                this.setState({
                    showmodal: false
                });
            }}
            confirmEvent={() => {
                this.cargarListado();
            }} ></AlertComponent>

        let listado = <DataTable
            columns={this.state.columnas}
            pagination
            data={this.state.productos} />;
        if (this.state.isLoading) {
            listado = <LoadingComponent></LoadingComponent>
        }
        return (
            <div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Productos</h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-success" onClick={() => { this.onclickAgrega() }}>
                            <FontAwesomeIcon icon="plus"></FontAwesomeIcon> Agregar producto
                        </span>
                    </div>
                    <div className="col-md-12">
                        {listado}
                    </div>
                </div>{mensaje}
            </div>
        );
    }
}

export default ListadoProducto;