import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Card from '../Elementos/Card'
import Ilustraciones from '../Elementos/Ilustraciones'

export default class Informes extends Component {
    state = {
        cards: [
            { name: 'Total de ventas', description: "1800", icon: 'tachometer-alt' },
            { name: 'Promociones', description: "2", icon: 'tachometer-alt' },
            { name: 'Informes', description: "1800", icon: 'tachometer-alt' },
            { name: 'Almacen', description: "2", icon: 'tachometer-alt' }
        ],
        ilustrations: [
            { texto: 'Texto ilustracion', titulo: 'Titulo ilustracion' }
        ]
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-4 text-gray-800">Administrador</h1>
                </div>
                <div className="row cards">
                    {this.state.cards.map((element, index) => {
                        return <Card key={index} nombre={element.name} description={element.description} icon={element.icon} />
                    })}
                </div>
                <div className="row">
                    <Ilustraciones texto={this.state.ilustrations[0].texto} titulo={this.state.ilustrations[0].titulo} ></Ilustraciones>
                </div>
            </div>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Informes />, document.getElementById('app'));
}
