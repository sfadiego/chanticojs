import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import AlertComponent from '../../AlertComponents/AlertComponent'
let GLOBAL = require("../../Core/Constantes");

class NuevoEstatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            estatus: '',
            icon: '',
            cssclass: '',
            color: '',
            showmodal: false,
            modal: { title: '', icon: GLOBAL.MODAL_SUCCESS, type: GLOBAL.MODAL_SUCCESS, text: "" },
            status_code: null,
        }
        this.handleSubmitevent = this.handleSubmitevent.bind(this);
    }

    onclickRegresa = () => {
        let path = `/admin/catalogo/estatus/`;
        this.props.history.push(path);
    }

    confirmModal() {

        if (this.state.status_code === 200) {
            let path = `/admin/catalogo/estatus/`;
            this.props.history.push(path);
        }

        this.setState({ showmodal: false });
    }

    handleSubmitevent(event) {
        event.preventDefault();
        if (!this.state.estatus || !this.state.icon || !this.state.cssclass || !this.state.color) {
            return this.handleModal(GLOBAL.FORM_INPUTS_REQUIRED, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400);
        }

        axios({
            url: `catestatuspedido/`,
            method: GLOBAL.POST,
            data: {
                estatus: this.state.estatus,
                icon: this.state.icon,
                cssclass: this.state.cssclass,
                color: this.state.color
            }
        }).then(response => this.handleModal(response.data.message, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200))
            .catch(error => this.handleModal(GLOBAL.ERROR_SAVING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400));
    }

    handleModal(text = GLOBAL.SUCCES, type = GLOBAL.MODAL_SUCCESS, title = GLOBAL.MODAL_SUCCESS, showmodal = true, status_code = null) {
        let dataOptions = {
            ...this.state.modal,
            text: text,
            type: type,
            title: title
        };

        return this.setState({
            modal: dataOptions,
            showmodal: showmodal,
            status_code: status_code
        });
    }


    render() {
        let mensaje = <AlertComponent type={this.state.modal.type}
            show={this.state.showmodal}
            title={(this.state.modal.title) ? this.state.modal.title : ''}
            text={this.state.modal.text}
            closeModal={() => this.setState({ showmodal: false })}
            confirmEvent={() => this.confirmModal()} ></AlertComponent>
        return (
            <div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Agregar Estatus</h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger btn-sm" onClick={() => { this.onclickRegresa(); }}>
                            <FontAwesomeIcon icon="arrow-left"></FontAwesomeIcon> Regresar
                        </span>
                    </div>
                </div>
                <div className="col-md-12">
                    <form onSubmit={this.handleSubmitevent}>
                        <div className="form-group">
                            <label htmlFor="estatus">Nombre del Estatus</label>
                            <select value={this.state.estatus} className="form-control" name="estatus" onChange={event => this.setState({ estatus: event.target.value })}>
                                <option value=""> Selecciona una estatus </option>
                                <option value="disponible"> disponible </option>
                                <option value="ocupado"> ocupado</option>
                                <option value="finalizado"> finalizado</option>
                                <option value="proceso"> proceso</option>
                            </select>
                            <span style={{ color: "red" }}></span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="color">Color</label>
                            <input type="text" className="form-control" name="color" disabled={this.state.input_disabled} id="color" onChange={event => this.setState({ color: event.target.value })} value={this.state.color} placeholder="Ejemplo: #1cc88a"></input>
                            <span style={{ color: "red" }}></span>
                            <small id="heltext" className="form-text text-muted">
                                Indica un color en formato exadecimal:
                                 https://htmlcolorcodes.com/es/
                                    <ul>
                                    <li style={{ 'color': '#1cc88a' }}>success: #1cc88a</li>
                                    <li style={{ 'color': '#f6c23e' }} >warning: #f6c23e</li>
                                    <li style={{ 'color': '#e74a3b' }} >Danger: #e74a3b </li>
                                    <li style={{ 'color': '#858796' }} >Secondary: #858796</li>
                                </ul>
                            </small>
                        </div>
                        <div className="form-group">
                            <label htmlFor="icono">Icono</label>
                            <select value={this.state.icon} className="form-control" name="icon" disabled={this.state.input_disabled} id="icon" onChange={event => this.setState({ icon: event.target.value })}>
                                <option value=""> Selecciona una icono </option>
                                <option value="check-circle"> check-circle</option>
                                <option value="hamburger"> hamburger</option>
                                <option value="clock"> clock</option>
                            </select>
                            <span style={{ color: "red" }}></span>
                            <small id="heltext" className="form-text text-muted">
                                Indica un icono para mostrar en estatus de mesa https://fontawesome.com/icons?d=gallery
                                </small>
                        </div>
                        <div className="form-group mb-5" >
                            <label htmlFor="exampleFormControlSelect1">Clase Css</label>
                            <select value={this.state.cssclass} className="form-control" disabled={this.state.input_disabled} id="cssclass" name="cssclass" onChange={event => this.setState({ cssclass: event.target.value })}>
                                <option value=""> Selecciona una clase </option>
                                <option value="border-left-success"> border - left - success </option>
                                <option value="border-left-danger"> border-left-danger</option>
                                <option value="border-left-warning"> border-left-warning</option>
                            </select>
                            <span style={{ color: "red" }}></span>
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary col-md-3">Guardar</button>
                        </div>
                    </form>
                    {mensaje}
                </div>
            </div>
        );
    }
}

export default NuevoEstatus;