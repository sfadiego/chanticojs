import React, { Component } from 'react';
import axios from 'axios';
import AlertComponent from '../../AlertComponents/AlertComponent'
import TableComponent from '../../Tablas/TableComponent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
let GLOBAL = require("../../Core/Constantes");

class catalogoEstatus extends Component {

    constructor(props) {
        super(props);
        this.state = {
            estatus_id: '',
            estatus: '',
            icon: '',
            cssclass: '',
            color: '',
            showmodal: false,
            modal: { title: '', icon: GLOBAL.MODAL_SUCCESS, type: GLOBAL.MODAL_SUCCESS, text: "" },
            listaEstatus: [],
            input_disabled: true,
            msg_error: {},
            status_code: null,
            isLoaded: false
        }
        this.handleSubmitevent = this.handleSubmitevent.bind(this);
    }

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} catalogoEstatus`);

    componentDidMount() {
        this.loadComponentInfo();
    }

    editElementHandler = data => {
        this.setState({ input_disabled: false, estatus_id: data });
        this.loadComponentInfo(data);
    }

    borrarElementoHandler = async (id) => {
        try {

            let url = `catestatuspedido/${id}`;
            let response = await axios.delete(url, { cancelToken: this.signal.token });
            this.handleModal(response.data.message, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200, true);

        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCELADA, error.message);
            } else {
                let errorResponse = error.response.data;
                this.handleModal(errorResponse.message + errorResponse.parametro, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400, true);
            }
        }
    }

    onclickAgrega = () => {
        let path = `/admin/catalogo/estatus/nuevo`;
        this.props.history.push(path);
    }

    loadComponentInfo = async (id = null) => {
        try {
            let endpoint = (id === null) ? 'catestatuspedido' : `catestatuspedido/${id}`
            let response = await axios.get(endpoint, { cancelToken: this.signal.token });
            let catalogo = response.data;

            if (id === null) {
                this.setState({ listaEstatus: catalogo, status_code: null, isLoaded: true });
            } else {

                this.setState({
                    estatus: catalogo.estatus,
                    icon: catalogo.icon,
                    cssclass: catalogo.cssclass,
                    color: catalogo.color,
                    status_code: null,
                    isLoaded: true
                });
            }

        } catch (error) {
            console.log(error)
        }
    }

    handleModal(text = GLOBAL.SUCCES, type = GLOBAL.MODAL_SUCCESS, title = GLOBAL.MODAL_SUCCESS, showmodal = true, status_code = null, input_disabled = false) {
        let dataOptions = {
            ...this.state.modal,
            text: text,
            type: type,
            title: title
        };

        return this.setState({
            modal: dataOptions,
            showmodal: showmodal,
            status_code: status_code,
            input_disabled: input_disabled
        });
    }


    handleSubmitevent = async (event) => {
        event.preventDefault();
        if (this.state.estatus_id === '') {
            return this.handleModal(GLOBAL.SELECT_ITEM, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, 400, true);
        }

        axios({
            url: `catestatuspedido/${this.state.estatus_id}`,
            method: GLOBAL.PUT,
            data: {
                estatus: this.state.estatus,
                icon: this.state.icon,
                cssclass: this.state.cssclass,
                color: this.state.color
            }
        }).then(success => {

            this.handleModal(GLOBAL.SUCCES, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, 200, true);

        }).catch(error => {
            let error_response = error.response.data;
            this.handleModal(error_response, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, error.response.status);

        });

    }

    confirmModal() {
        switch (this.state.status_code) {
            case 400:
                this.setState({ showmodal: false });
                break;
            default:
                this.loadComponentInfo(null);
                this.setState({ showmodal: false });
                break;
        }
    }

    render() {

        let columns = ['#', 'estatus', 'icon', '--'];
        let mensaje = <AlertComponent type={this.state.modal.type}
            show={this.state.showmodal}
            title={(this.state.modal.title) ? this.state.modal.title : ''}
            text={this.state.modal.text}
            closeModal={() => {
                this.setState({
                    showmodal: false
                });
            }}
            confirmEvent={() => {
                this.confirmModal();
            }} ></AlertComponent>
        return (
            <div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Agregar Estatus</h1>
                </div>

                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-md-10"></div>
                            <div className="col-md-2 text-right pr-2">
                                <span className="btn btn-success btn-sm" onClick={() => { this.onclickAgrega() }}>
                                    <FontAwesomeIcon icon="plus"></FontAwesomeIcon> Agregar estatus
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <form onSubmit={this.handleSubmitevent}>
                            <div className="form-group">
                                <label htmlFor="estatus">Nombre del Estatus</label>
                                <select value={this.state.estatus} className="form-control" id="estatus" disabled={this.state.input_disabled} name="estatus" onChange={event => this.setState({ estatus: event.target.value })}>
                                    <option value=""> Selecciona una estatus </option>
                                    <option value="disponible"> disponible </option>
                                    <option value="ocupado"> ocupado</option>
                                    <option value="finalizado"> finalizado</option>
                                    <option value="proceso"> proceso</option>
                                </select>
                                <span style={{ color: "red" }}>{this.state.msg_error['estatus']}</span>
                            </div>
                            <div className="form-group">
                                <label htmlFor="color">Color</label>
                                <input type="text" className="form-control" name="color" disabled={this.state.input_disabled} id="color" onChange={event => this.setState({ color: event.target.value })} value={this.state.color} placeholder="Ejemplo: #1cc88a"></input>
                                <span style={{ color: "red" }}>{this.state.msg_error['color']}</span>
                                <small id="heltext" className="form-text text-muted">
                                    Indica un color en formato exadecimal:
                                     https://htmlcolorcodes.com/es/
                                    <ul>
                                        <li style={{'color':'#1cc88a'}}>success: #1cc88a</li>
                                        <li style={{'color':'#f6c23e'}} >warning: #f6c23e</li>
                                        <li style={{'color':'#e74a3b'}} >Danger: #e74a3b </li>
                                        <li style={{'color':'#858796'}} >Secondary: #858796</li>
                                    </ul>
                                </small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="icono">Icono</label>
                                <select value={this.state.icon} className="form-control" name="icon" disabled={this.state.input_disabled} id="icon" onChange={event => this.setState({ icon: event.target.value })}>
                                    <option value=""> Selecciona una icono </option>
                                    <option value="check-circle"> check-circle</option>
                                    <option value="hamburger"> hamburger</option>
                                    <option value="clock"> clock</option>
                                </select>
                                <span style={{ color: "red" }}>{this.state.msg_error['icon']}</span>
                                <small id="heltext" className="form-text text-muted">
                                    Indica un icono para mostrar en estatus de mesa https://fontawesome.com/icons?d=gallery
                                </small>
                            </div>
                            <div className="form-group mb-5" >
                                <label htmlFor="exampleFormControlSelect1">Clase Css</label>
                                <select value={this.state.cssclass} className="form-control" disabled={this.state.input_disabled} id="cssclass" name="cssclass" onChange={event => this.setState({ cssclass: event.target.value })}>
                                    <option value=""> Selecciona una clase </option>
                                    <option value="border-left-success"> border - left - success </option>
                                    <option value="border-left-danger"> border-left-danger</option>
                                    <option value="border-left-warning"> border-left-warning</option>
                                </select>
                                <span style={{ color: "red" }}>{this.state.msg_error['cssclass']}</span>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary col-md-3">Guardar</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <TableComponent editar={this.editElementHandler} borrar={this.borrarElementoHandler}
                            columnas={columns}
                            nombre_item1={'estatus'}
                            nombre_item2={'icon'}
                            isLoaded={this.state.isLoaded}
                            data={this.state.listaEstatus} />
                    </div>
                </div>
                {mensaje}
            </div>
        );
    }
}

export default catalogoEstatus;