import React, { Component } from 'react';
import AlertComponent from '../../AlertComponents/AlertComponent';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
let  GLOBAL = require("../../Core/Constantes");

class Categorias extends Component {
    constructor(props) {
        super(props);
        this.state ={
            showmodal: false,
            modal: { title: '', icon: "success", type: "success", text:""},
            activo:true,
            categoria:"",
            msg_error: {},
        }
        this.handleSubmitevent = this.handleSubmitevent.bind(this);
        this.handleConfirmModal = this.handleConfirmModal.bind(this);
    }

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} categorias`);

    onclickRegresa = () => {
        let path = `/admin/catalogo/categorias/`;
        this.props.history.push(path);
    }

    handleConfirmModal(){
        let path = `/admin/catalogo/categorias`
        this.props.history.push(path);
    }
    
   handleSubmitevent = async(event)=> {
    event.preventDefault();
    try {
        let response = await axios.post( 'categoria',{
                    categoria: this.state.categoria,
                    activo: this.state.activo }, {cancelToken: this.signal.token });
        let data ={
            ...this.state.modal,
            text:response.data.message,
            icon:GLOBAL.SUCCES,
            type:GLOBAL.MODAL_SUCCESS,
            title:GLOBAL.SUCCES
        }
        this.setState({modal:data, showmodal:true});

    } catch (error) {
        if (axios.isCancel(error)) {
            console.log(GLOBAL.API_CANCELADA, error.message);
        }else{
            let errorResponse = error.response.data;
            let data ={
                ...this.state.modal,
                text:errorResponse.message + errorResponse.parametro,
                icon:GLOBAL.MODAL_ERROR,
                type:GLOBAL.MODAL_ERROR
            }
            this.setState({modal:data, show:true});

        }
    }
}
    
    render() {
        let mensaje = <AlertComponent type={this.state.modal.type} 
                                    show={this.state.showmodal} 
                                    title = {this.state.modal.title}  
                                    text={this.state.modal.text} 
                                    closeModal={()=>{
                                        this.setState({
                                            showmodal: false
                                        });
                                    }}
                                    confirmEvent={()=>{ this.handleConfirmModal() }} ></AlertComponent>
        return (
            <div className = "container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Agregar Categoria</h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger btn-sm" onClick={()=>{ this.onclickRegresa(); }}> 
                            <FontAwesomeIcon icon="arrow-left"></FontAwesomeIcon> Regresar
                        </span>
                    </div>
                </div> 
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={this.handleSubmitevent}>
                             <div className="form-group">
                                <label htmlFor="producto">Nombre de categoria</label>
                                <input type="text" placeholder="Ejemplo: Cafés" className="form-control" name="categoria" id="categoria" onChange={event => this.setState({categoria:event.target.value}) }></input>
                                <span style={{color: "red"}}>{this.state.msg_error['categoria']}</span>
                            </div>
                            <div className="form-check mb-3">
                                <input className="form-check-input" checked onChange={event => this.setState({activo: (event.target.value) ? true: false}) } type="checkbox"></input>
                                <label className="form-check-label" htmlFor="valido">
                                    Válido
                                </label>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary col-md-3">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
                {mensaje}
            </div>       
        );
    }
}

export default Categorias;