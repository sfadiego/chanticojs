import React, { Component } from 'react';
import TableComponent from '../../Tablas/TableComponent';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AlertComponent from '../../AlertComponents/AlertComponent';
let GLOBAL = require("../../Core/Constantes");

class Listado extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showmodal: false,
            modal: {
                title: null,
                icon: GLOBAL.MODAL_SUCCESS,
                type: GLOBAL.MODAL_SUCCESS,
                text: GLOBAL.SUCCES_MESSAGE
            },
            activo: true,
            categoria: '',
            categoria_id: null,
            msg_error: [],
            categorias: [],
            input_disabled: true,
            isLoaded: false
        }
        this.handleSubmitevent = this.handleSubmitevent.bind(this);
        this.handleConfirmModal = this.handleConfirmModal.bind(this);
    }

    signal = axios.CancelToken.source();

    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} Listado Producto`);

    componentDidMount() {
        this.cargarListado();
    }

    cargarListado = async (id = null) => {
        try {
            let endpoint = (id === null) ? 'categoria' : `categoria/${id}`
            let listado = await axios.get(endpoint, {
                cancelToken: this.signal.token
            });

            if (id !== null) {
                let categoria = listado.data;
                this.setState({
                    categoria_id: categoria._id,
                    categoria: categoria.categoria,
                    activo: categoria.activo,
                    isLoaded: true
                });

            } else {
                let categorias = listado.data;
                this.setState({
                    categorias: categorias,
                    isLoaded: true

                });
            }
        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            }
        }

    }

    editElementHandler = data => {
        this.cargarListado(data);
        this.setState({ input_disabled: false });
    }

    handleConfirmModal() {
        this.cargarListado();
        this.setState({ showmodal: false });

    }

    handleModal(text = GLOBAL.SUCCES, type = GLOBAL.MODAL_SUCCESS, title = GLOBAL.MODAL_SUCCESS, showmodal = true, input_disabled = false) {
        let dataOptions = {
            ...this.state.modal,
            text: text,
            type: type,
            title: title
        };

        return this.setState({
            modal: dataOptions,
            showmodal: showmodal,
            input_disabled: input_disabled
        });
    }

    borrarElementoHandler = async (id) => {
        try {

            let url = `categoria/${id}`;
            let response = await axios.delete(url, { cancelToken: this.signal.token });
            this.handleModal(response.data.message, GLOBAL.MODAL_SUCCESS, GLOBAL.MODAL_SUCCESS, true, true);

        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCELADA, error.message);
            } else {
                let errorResponse = error;
                console.log(errorResponse);
                this.handleModal(GLOBAL.ERROR_DELETING, GLOBAL.MODAL_ERROR, GLOBAL.MODAL_ERROR, true, true);
            }
        }
    }

    onclickAgrega = () => {
        let path = `/admin/catalogo/categoria/nueva`
        this.props.history.push(path);
    }

    handleSubmitevent = async (event) => {
        event.preventDefault();

        try {
            let url = `categoria/${this.state.categoria_id}`;
            await axios.put(url, {
                categoria: this.state.categoria,
                activo: this.state.activo,
            }, { cancelToken: this.signal.token });

            this.setState({
                showmodal: true,
                input_disabled: true,
                categorias: []
            }, function () {
                this.cargarListado();
            });

        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            } else {
                console.log(error);
            }
        }
    }



    render() {
        let columns = ['#', 'Categoria', '--'];
        let mensaje = <AlertComponent type={this.state.modal.type}
            show={this.state.showmodal}
            title={this.state.modal.title}
            text={this.state.modal.text}
            closeModal={() => {
                this.setState({
                    showmodal: false
                });
            }}
            confirmEvent={() => { this.handleConfirmModal() }} ></AlertComponent>
        return (
            <div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Categorias</h1>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-md-10"></div>
                            <div className="col-md-2 text-right pr-2">
                                <span className="btn btn-success btn-sm" onClick={() => { this.onclickAgrega() }}>
                                    <FontAwesomeIcon icon="plus"></FontAwesomeIcon> Agregar categoria
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <form onSubmit={this.handleSubmitevent}>
                            <div className="form-group">
                                <label htmlFor="producto">Nombre de categoria</label>
                                <input disabled={this.state.input_disabled} type="text" placeholder="Ejemplo: Café" value={this.state.categoria} className="form-control" name="categoria" id="categoria" onChange={event => this.setState({ categoria: event.target.value })}></input>
                                <span style={{ color: "red" }}>{this.state.msg_error['categoria']}</span>
                            </div>
                            <div className="form-check mb-3">
                                <input disabled={(this.state.input_disabled) ? `disabled` : ''}
                                    className="form-check-input"
                                    checked={this.state.activo}
                                    value={this.state.activo}
                                    onChange={event => {
                                        let target = event.target.checked;
                                        this.setState({ activo: target });
                                    }} type="checkbox"></input>

                                <label className="form-check-label" htmlFor="valido">
                                    Válido
                                </label>
                            </div>
                            {
                                (this.state.input_disabled) ?
                                    '' :
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-primary col-md-3">Actualizar</button>
                                    </div>
                            }

                        </form>
                    </div>
                    <div className="col-md-6">
                        <TableComponent editar={this.editElementHandler} borrar={this.borrarElementoHandler}
                            columnas={columns}
                            nombre_item1={'categoria'}
                            nombre_item2={null}
                            isLoaded={this.state.isLoaded}
                            data={this.state.categorias} />
                    </div>
                </div>{mensaje}
            </div>
        );
    }
}

export default Listado;