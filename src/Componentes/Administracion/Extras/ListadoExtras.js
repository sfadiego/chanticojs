import React, { Component } from 'react'
import DataTable from 'react-data-table-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import * as TablaExtras from './TablaColumnas'
import {Link} from 'react-router-dom';
let GLOBAL = require("../../Core/Constantes");


export default class ListadoExtras extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columnas:[],
            data:[]
        }
    }

    signal = axios.CancelToken.source();
    
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        let columnas = TablaExtras.createColumnasExtras();
        columnas.push({
                name: '--',
                sortable: true,
                center: true,
                cell:row => {
                    return <Link className="btn btn-primary btn-sm" to={row.url}>  
                        <FontAwesomeIcon icon="edit"></FontAwesomeIcon> 
                    </Link>
                }
            });
        let data = TablaExtras.getItemsExtras();
        this.setState({
            columnas:columnas,
            data:data
        });
    }
    
    render() {
        let columnas = this.state.columnas;
        let data = this.state.data
        return (
            <div className = "container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Complementos</h1>
                </div> 
                <div className="row">
                    <div className="col-md-12">
                        <DataTable
                            columns={columnas}
                            pagination
                            data={data} />
                    </div>
                </div>
            </div>
        )
    }
}
