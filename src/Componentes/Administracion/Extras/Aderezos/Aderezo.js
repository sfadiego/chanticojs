import React, { Component } from 'react'
import DataTable from 'react-data-table-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import * as TablaExtras from '../TablaColumnas'
import {Link} from 'react-router-dom';
import LoadingComponent from '../../../Elementos/LoadingComponent';
let GLOBAL = require("../../../Core/Constantes");

export default class Aderezo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columnas:[],
            data:[],
            isLoading:false,
        }
    }

    signal = axios.CancelToken.source();
    
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        try {
            this.loadData();            
        } catch (error) {
            if (axios.isCancel(error)) return console.log(GLOBAL.API_CANCEL_REQUEST, error.message); 
        }
    }

    loadData = async ()=>{
        try {
            let aderezos = await axios.get(`aderezos`,{cancelToken:this.signal.token});
            let columnas = TablaExtras.createColumnasAderezos();
            columnas.push({
                    name: '--',
                    sortable: true,
                    center: true,
                    cell:row => {
                        let url = `/admin/aderezo/${row._id}`;
                        return <Link className="btn btn-primary btn-sm" to={url}>  
                            <FontAwesomeIcon icon="edit"></FontAwesomeIcon> 
                        </Link>
                    }
                });
            
            this.setState({data:aderezos.data, columnas:columnas});
            
        } catch (error) {
            if (axios.isCancel(error)) return console.log(GLOBAL.API_CANCEL_REQUEST, error.message); 
            console.log(error);   
        }
    }

    regresar(){
       return this.props.history.push('/admin/complementos/');
    }
    
    render() {
        let columnas = this.state.columnas;
        let data = this.state.data
        return (
            <div className = "container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Aderezos</h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger" onClick={()=> this.regresar() }> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>
                        </span>
                    </div>
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right mt-2">
                        <span className="btn btn-success" onClick={()=>{ 
                            this.props.history.push("/admin/crear/aderezo/") }}> 
                            <FontAwesomeIcon icon="plus"></FontAwesomeIcon> Agregar
                        </span>
                    </div>
                </div>
                {
                    !this.state.isLoading ? 
                    <div className="row">
                        <div className="col-md-12">
                            <DataTable
                                columns={columnas}
                                pagination
                                data={data} />
                        </div>
                    </div>
                    :<LoadingComponent></LoadingComponent>
                }
            </div>
        )
    }
}
