import React, { Component } from 'react'
import DataTable from 'react-data-table-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import * as TablaExtras from '../TablaColumnas'
import {Link} from 'react-router-dom';
import * as Funciones from '../../../Core/Funcions'
let GLOBAL = require("../../../Core/Constantes");


export default class Sabores extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columnas:[],
            data:[]
        }
    }

    signal = axios.CancelToken.source();
    
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        let columnas = TablaExtras.createColumnasSabores();
        columnas.push({
                name: '--',
                sortable: true,
                center: true,
                cell:row => {
                    let url = `/admin/sabor/${row._id}`;
                    return <Link className="btn btn-primary btn-sm" to={url}>  
                        <FontAwesomeIcon icon="edit"></FontAwesomeIcon> 
                    </Link>
                }
            });
        this.setState({ columnas:columnas});
        this.loadData();
    }
    
    loadData = async ()=>{
        let sabores = await Funciones.getDataAxios('sabores', this.signal.token)
        this.setState({data:sabores});
    }
    
    render() {
        let columnas = this.state.columnas;
        let data = this.state.data
        return (
            <div className = "container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Sabores</h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger" onClick={()=> this.props.history.push('/admin/complementos/')}> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>
                        </span>
                    </div>
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right mt-2">
                        <span className="btn btn-success" onClick={()=>{ 
                            this.props.history.push("/admin/crear/sabor") }}> 
                            <FontAwesomeIcon icon="plus"></FontAwesomeIcon> Agregar
                        </span>
                    </div>
                </div> 
                <div className="row">
                    <div className="col-md-12">
                        <DataTable
                            columns={columnas}
                            pagination
                            data={data} />
                    </div>
                </div>
            </div>
        )
    }
}
