export function createColumnasExtras(){
    return [
        {
            name: '#',
            sortable: true,
            selector:'id'
        },
        {
            name: 'Complemento',
            selector: 'complemento',
            sortable: true,
            center: true,
        },
    ]; 
}


export function getItemsExtras (){
    return [{            
            id:1,
            complemento:"Sabores",
            url:"/admin/complementos/sabor",
        },
        {
            id:2,
            complemento:"Aderezos",
            url:"/admin/complementos/aderezo",
        },
        {
            id:3,
            complemento:"Proteinas",
            url:"/admin/complementos/proteina",
        },
        {
            id:4,
            complemento:"Tipo de bebidas",
            url:"/admin/complementos/bebidas",
        },
        {
            id:5,
            complemento:"Sabores dulces",
            url:"/admin/complementos/saboresdulces",
        }
    ];
}


export function createColumnasAderezos(){
    return [
        {
            name: '#',
            sortable: true,
            selector:'_id'
        },
        {
            name: 'Nombre',
            selector: 'nombre',
            sortable: true,
            center: true,
        },
        {
            name: 'Precio',
            selector: 'precio',
            sortable: true,
            center: true,
        },
    ]; 
}

export function createColumnasSabores(){
    return [
        {
            name: '#',
            sortable: true,
            selector:'_id'
        },
        {
            name: 'Nombre',
            selector: 'nombre',
            sortable: true,
            center: true,
        },
        {
            name: 'Precio',
            selector: 'precio',
            sortable: true,
            center: true,
        },
    ]; 
}

export function createColumnasBebidas(){
    return [
        {
            name: '#',
            sortable: true,
            selector:'_id'
        },
        {
            name: 'Nombre',
            selector: 'nombre',
            sortable: true,
            center: true,
        },
        {
            name: 'Precio',
            selector: 'precio',
            sortable: true,
            center: true,
        },
    ]; 
}

export function createColumnasProteinas(){
    return [
        {
            name: '#',
            sortable: true,
            selector:'_id'
        },
        {
            name: 'Nombre',
            selector: 'nombre',
            sortable: true,
            center: true,
        },
        {
            name: 'Precio',
            selector: 'precio',
            sortable: true,
            center: true,
        },
    ]; 
}