import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Funciones from '../../../Core/Funcions'
import LoadingComponent from '../../../Elementos/LoadingComponent'
let GLOBAL = require("../../../Core/Constantes");

export default class RegistrarEdita extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nombre:'',
            precio:0,
            proteina_id:null,
            isLoadingData:true
        }
        this.handleSubmitevent = this.handleSubmitevent.bind(this);
    }

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount(){
        const id = this.props.match.params.id;
        this.setState({isLoadingData:false});
        if(id) this.loadData();
    }

    loadData = async () => {
        try {
            const id = this.props.match.params.id;
            await axios.get(`proteina/${id}`,{cancelToken:this.signal.token})
            .then(success=>{
                let data = success.data;
                this.setState({
                    proteina_id:id,
                    nombre:data.nombre,
                    precio:data.precio,
                    isLoadingData:false
                });
            }).catch(error=> console.log(error.message));
            
        } catch (error) {
            if (axios.isCancel(error)) return console.log(GLOBAL.API_CANCEL_REQUEST, error.message);   
        }
    }

    handleSubmitevent(event){
        event.preventDefault();
        let data = {
            nombre:this.state.nombre,
            precio:this.state.precio
        }
        let method = (this.state.proteina_id) ? GLOBAL.PUT : GLOBAL.POST
        let url = (this.state.proteina_id) ? `proteina/${this.state.proteina_id}` : `proteina/`
        Funciones.requestAxios(url,method, data)
        .then(success=>  this.props.history.push("/admin/complementos/proteina/"))
        .catch(error => console.log(error));
    }

    render() {
        return (
            <div className = "container-fluid">
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">{this.state.proteina_id ? this.state.nombre : "Nueva proteina"}</h1>
                </div> 
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right pr-2">
                        <span className="btn btn-danger" onClick={()=>{ this.props.history.push("/admin/complementos/proteina/") }}> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>
                        </span>
                    </div>
                    {
                        !this.state.isLoadingData ? 
                        <div className="col-md-12">
                            <form onSubmit={this.handleSubmitevent}>
                                <div className="form-group">
                                    <label htmlFor="producto">Nombre <span style={{color: "red"}}>*</span></label>
                                    <input type="text" className="form-control" value={this.state.nombre} onChange={event => this.setState({nombre:event.target.value}) }></input>
                                </div> 
                                <div className="form-group">
                                    <label htmlFor="producto">Precio <span style={{color: "red"}}>*</span></label>
                                    <input type="number" className="form-control" value={this.state.precio} onChange={event => this.setState({precio:event.target.value}) }></input>
                                </div>
                                <div className="form-group">
                                    <button type="submit"  className="btn btn-primary col-md-3">{!this.state.proteina_id ? "Guardar":"Actualizar" }</button>
                                </div> 
                            </form>
                        </div>
                        :<div className="col-md-12">
                            <LoadingComponent />
                        </div>
                    }
                </div>
            </div>
        )
    }
}
