import React, { Component } from 'react'
import axios from 'axios';
let GLOBAL = require("../Core/Constantes");

export default class Categorias extends Component {
    state = {
        categorias: [],
        selectedCategoria: null
    };

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} Categorias `);

    componentDidMount() {
        axios.get('categoria', { cancelToken: this.signal.token })
            .then(response => {
                const objcategoria = response.data;
                this.setState({ categorias: objcategoria });
            }).catch(error => {
                if (axios.isCancel(error)) {
                    console.log(GLOBAL.API_CANCELADA, error.message);
                }
            });
    }

    seleccionaCategorias = (id) => {
        this.setState({ selectedCategoria: id })
        this.props.getParentData(id);
    }

    render() {
        return (
            <ul className="nav nav-tabs" id="myTab" role="tablist">
                {
                    this.state.categorias.map((item, key) => {
                        return (
                            <li className="nav-item border-right-primary-tabs" onClick={() => this.seleccionaCategorias(item._id)} key={key}>
                                <span className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">{item.categoria.toUpperCase()}</span>
                            </li>

                        )
                    })
                }
            </ul>
        )
    }
}