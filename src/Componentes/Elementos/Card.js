import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Card extends Component {
  
  agregarMesa =(data) =>{
    this.props.bindData(data);
  }
  
  render() {
    let status = this.props.status;
    
    return (
      <div className="col-xl-3 col-md-6 mb-4">
        <div onClick={() => { this.agregarMesa(this.props) } }  className={"card shadow h-100 py-2 cursor-active " + status.cssclass} >
          <div className="card-body">
            <div className="row no-gutters align-items-center">
              <div className="col mr-2">
                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">Nombre: {(this.props.data.mesa) ? this.props.data.mesa : '' } </div>
                <div className="h5 mb-0 font-weight-bold text-gray-800">{ status.estatus }</div>
              </div>
              <div className="col-auto">
                <i className="text-gray-300 fa-2x "> 
                <FontAwesomeIcon color={ status.color } icon={ status.icon }/> 
                </i>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
