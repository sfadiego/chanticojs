import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Styles/Styles.css';
import * as Funciones from '../../Core/Funcions'
export default class RenderItems extends Component {
    
    extras(){
        let data = this.props.item;
        return  Funciones.calculaTotalExtrasxRow(data);
    }

    preciototalxRow(){
        let descuento = this.props.item.descuento ? this.props.item.descuento : 0;
        let total = Funciones.calculaExtrasYproductoxRow(this.props.item);
        return this.props.item.descuento !== 0 ? Funciones.calculaTotalConDescuento(total,descuento) : total;
    }

    borrarTodoElementoRow=(data)=> this.props.borrarTodoElementoRow(data);

    menosUnItem=(id)=>{
        if(this.props.item.cantidad <= 1 ){
            this.props.borrarTodoElementoRow(id);
            return false;
        }
        this.props.menosUnItem(this.props.item);
    }
    
    masUnElemento=()=>this.props.masUnElemento(this.props.item);

    validateExtras = () =>this.props.editarExtrasEnComanda(this.props.item);

    render() {
        let extras = this.props.item.extras ? this.props.item.extras : [];
        let productoCantidad = this.props.item.cantidad;
        let descuento = this.props.item.descuento > 0 ? <span className="text-danger text-xs">`-{this.props.item.descuento}%`</span> : <FontAwesomeIcon size="1x" icon="tag" color="#f0bf18"></FontAwesomeIcon>;
        let precio_extras = this.extras();
        let precioxrow = this.preciototalxRow();
        return (
            <div className={`row item_container small`} >
                <div onClick={()=> !this.props.isLoading ? this.validateExtras(): null } className={!this.props.isLoading ? "col-md-5" : "col-md-6"  }>
                    <div className="titulo-comanda">
                        <span>{this.props.item.producto}</span>
                    </div>
                    <div className="ml-2">
                        {   
                            extras.map((item, key)=>{
                                let div1 = <div className="col-12 col-sm-12 text-xs" > { item.nombre } ${item.precio}  </div>
                                return <div key={key} className="row">{div1}</div>
                            })
                        }
                    </div>    
                </div>
                <div className="col-md-4 text-center">
                    <div className="row">
                        {
                            !this.props.isLoading ? 
                            <div className="col-md-3 col-sm-4 col-4" onClick={() => { this.menosUnItem(this.props.item)}} >
                                <FontAwesomeIcon icon="minus"></FontAwesomeIcon>
                            </div> : null
                        }
                        <div className={!this.props.isLoading ? "col-md-6 col-sm-5 col-4" : "col-md-12 col-12 col-sm-12"}> x {productoCantidad} </div>
                        {
                            !this.props.isLoading ? 
                            <div className="col-md-3 col-sm-3 col-4" onClick={() => { this.masUnElemento()}} >
                                <FontAwesomeIcon icon="plus"></FontAwesomeIcon>
                            </div> : null
                        }
                        <div className="col-md-12 col-12 col-sm-12"> $ { precio_extras }</div>
                    </div>
                </div>
                <div className="col-md-2 text-center">
                    <div className="row" onClick={()=> !this.props.isLoading ? this.props.handleDetalleItem(this.props.item) : null }>
                            <div className="col-md-12">$ {  precioxrow }</div>
                            <div className="col-md-12">
                                { descuento}
                            </div>
                    </div>
                </div>
                {
                    !this.props.isLoading ? 
                    <div className="col-md-1 text-center" onClick={() => { this.borrarTodoElementoRow(this.props.item)}} >
                        <FontAwesomeIcon  icon="times" color="red"></FontAwesomeIcon>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}
