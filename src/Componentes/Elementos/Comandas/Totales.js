import React, { Component } from 'react'

class Totales extends Component {
  render() {
    return (
      <div className="row padding-totales">
          <div className="col-md-4 col-4 col-sm-4">
              <span>Total: </span>
          </div>
          <div className="col-md-8 col-8 col-sm-8 text-right">
            <h5>$ { this.props.total } </h5> 
          </div>
      </div>
    )
  }
}
export default Totales;
