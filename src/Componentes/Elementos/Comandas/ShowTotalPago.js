import React, { Component } from 'react'
import { Animated } from 'react-animated-css';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TotalPagar from '../../Comandas/TotalPagar';

export default class componentName extends Component {
    render() {
        return (
            <Animated animationOut="fadeOut" isVisible={this.props.isVisible} animationInDuration={400}>
                <div className="row">
                </div>
                <TotalPagar
                    cambio={this.props.totalCambio}
                    total={this.props.total} />
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <Button className="btn btn-lg col-md-6 btn-primary br-0 mt-3" onClick={()=>this.props.procesar()}> Terminar </Button>
                        <Button className="btn btn-lg btn-danger col-md-6 br-0 mt-3" onClick={()=>this.props.show()}> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>  
                        </Button>
                    </div>
                    <div className="col-md-3"></div>

                </div>
            </Animated>
        )
    }
}
