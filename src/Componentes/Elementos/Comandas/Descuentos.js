import React, { Component } from 'react'

class Descuentos extends Component {
  
  render() {
    return ( 
      <div className="row padding-totales">
          <div className="col-md-4 col-4">
              <span>Descuento: </span>
          </div>
          <div className="col-md-8 col-8 text-right">
            <h6> {this.props.descuento}%</h6>
          </div>
      </div>
    )
  }
}
export default Descuentos;