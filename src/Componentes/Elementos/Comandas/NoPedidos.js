import React, { Component } from 'react'

export default class NoPedidos extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-12 pr-0">
                    <div className="card mt-2 border-bottom-danger text-center">
                        <div className="card-body">
                            <h6>El pedido esta vacio</h6>
                            <h6>Agregar productos al pedido</h6>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
