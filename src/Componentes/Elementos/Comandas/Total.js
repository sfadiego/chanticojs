import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class componentName extends Component {
    render() {
        return (
            <div className="col-md-12 totales pt-1">
                
                <div className="container-totales">
                    <div className="container-item">Pedido: </div>
                    <div className="container-item text-right text-primary" style={{fontWeight:700}}>{this.props.nombrepedido}</div>
                </div>
                <div className="container-totales">
                    <div className="container-item">Descuento: </div>
                    <div className="container-item text-right">{this.props.descuento} %</div>
                </div>
                <div className="container-totales">
                    <div className="container-item">Subtotal: </div>
                    <div className="container-item text-right">$ {this.props.subtotal}</div>
                </div>
                <div className="container-totales">
                    <div className="container-item">Total: </div>
                    <div className="container-item text-right text-danger">$ {this.props.total}</div>
                </div>
                <div>
                <button disabled={this.props.isLoading}  onClick={this.props.agregarDescuento}  className="btn btn-success col-md-6 br-0 button-tickes mt-2"> 
                    <FontAwesomeIcon icon="percentage"></FontAwesomeIcon> Descuento 
                </button>
                <button disabled={this.props.isLoading }  onClick={this.props.guardarPedido} type="button"  className="btn mt-2 btn-primary col-md-6 br-0 br-1 button-tickes"> 
                    <FontAwesomeIcon icon="utensils"></FontAwesomeIcon> Guardar
                </button>
                <button disabled={this.props.isLoading}  onClick={this.props.pagarComanda} type="button"  className="btn mt-2 btn-danger col-md-12 br-0 br-1 button-tickes"> 
                    <FontAwesomeIcon icon="calculator"></FontAwesomeIcon> Pagar
                </button>
                </div>
            </div>
        )
    }
}
