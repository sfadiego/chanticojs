import React, { Component } from 'react'

export default class Subtotal extends Component {
  render() {
    return (
      <div className="row padding-totales">
        <div className="col-md-4 col-4 col-sm-4">
            <span>Subtotal: </span>
        </div>
        <div className="col-md-8 col-8 col-sm-8 text-right">
            <span> $ {this.props.subtotal} </span>
        </div>
      </div>

    )
  }
}
