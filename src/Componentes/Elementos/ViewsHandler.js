const CALCULADORA = 1;
const PRODUCTOS = 2;
const TOTAL_PAGOS = 3;
const EXTRAS = 4;
const READONLY = 5;
const DESCUENTO = 6;
const DESCUENTO_TOTAL = 7;
export function views(){
    return {
        CALCULADORA,
        PRODUCTOS,
        TOTAL_PAGOS,
        EXTRAS,
        READONLY,
        DESCUENTO,
        DESCUENTO_TOTAL,
    }
}

export function viewHandler(view){
    if (view === CALCULADORA) {
        return {
            calculadora:true,
            productos:false,
            extras:false,
            loading:true,
            total_pagar:false
        }
    }else if(view === PRODUCTOS){
        return {
            calculadora:false,
            productos:true,
            extras:false,
            total_pagar:false
        }
    }else if(view === TOTAL_PAGOS){
        return {
            calculadora:false,
            productos:false,
            extras:false,
            total_pagar:true
        }
    }else if(view === EXTRAS){
        return {
            calculadora:false,
            productos:false,
            extras:true,
            total_pagar:false
        }
    }else if(view === READONLY){
        return {
            calculadora:true,
            productos:false,
            extras:false,
            total_pagar:false
        }
    }else if(view === DESCUENTO){
        return {
            calculadora:false,
            productos:false,
            extras:false,
            total_pagar:false,
            showItemDetail:true
        }
    }else if (view === DESCUENTO_TOTAL){
        return {
            calculadora:false,
            productos:false,
            extras:false,
            total_pagar:false,
            showViewDescuento:true
        }
    }
    else{
        return {
            calculadora:false,
            productos:true,
            extras:false,
            total_pagar:false
        } 
    }
}