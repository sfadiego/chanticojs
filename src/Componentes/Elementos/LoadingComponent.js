import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class LoadingComponent extends Component {
    render() {
        return (
            <div className="row text-center">
                <div className="col-md-12 mt-3">
                    <FontAwesomeIcon color={this.props.color ? `${this.props.color}` : "#4e73df"} pulse icon="spinner" size={this.props.size ? `${this.props.size}` : "6x"} />
                </div>
            </div>
        )
    }
}
