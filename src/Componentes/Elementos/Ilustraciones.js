import React, { Component } from 'react'
import logo from '../../Resources/img/logo_chantico_secundario.png'
export default class Ilustraciones extends Component {
    
    render() {
        const style ={
            width: '25rem'
        };

    return (
        <div className="col-lg-3 mb-4">
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <h6 className="m-0 font-weight-bold text-primary">{this.props.titulo}</h6>
                </div>
                <div className="card-body">
                    <div className="text-center">
                        <img className="img-fluid px-3 px-sm-4 mt-3 mb-4" style={style} src={logo} alt="">
                        </img>
                    </div>
                    <p>{this.props.texto}</p>
                    <a onClick={(event)=>{alert("Agregado a la comanda");}} rel="nofollow" href="https://undraw.co/">Detalles →</a>
                </div>
            </div>
        </div>
    )
  }
}
