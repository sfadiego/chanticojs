import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
let GLOBAL = require("../Core/Constantes");

export default class CardSaborDulce extends Component {
    constructor(props) {
        super(props);
        this.state ={
            items_ocupados:[],
            producto_id: null,
            catalogo:null,
            isloading:false
        };
        
    }
    signal = axios.CancelToken.source();
    
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} Sabores`);

    componentDidMount() {
        this.cargaCatalogo();
    }

    componentDidUpdate(prevProps){
        if(this.state.producto_id !== prevProps.producto_id){
            this.setState({producto_id:prevProps.producto_id})
            this.getRelacionSaborProducto(prevProps.producto_id);
        }
    }

    getRelacionSaborProducto = async (producto_id) =>{
        
        await axios.get( `productos/getrelacionproductossaboresdulcesbyProducto/${producto_id}`, { cancelToken: this.signal.token })
        .then((result) => {
            let response = result.data;
            this.setState({items_ocupados:response})

            let catalogo = this.state.catalogo;
            catalogo.map((item_sabor,key) =>{
                let getIndex = response.findIndex(item => item.sabor_id === item_sabor._id);                
                return this.setState(state =>{
                      let item_orig = state.catalogo[key];
                      let item = item_orig['agregado'] = (getIndex === -1) ? false : true;
                      return item
                })
            });
            this.setState({isloading:false});
        }).catch((err) =>  console.log(err));
    }

    cargaCatalogo = async () => {
        try {
            let listado =  await axios.get( 'saboresdulces', { cancelToken: this.signal.token });
            let response_catalogo = listado.data
            this.setState({ catalogo:response_catalogo });
        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(GLOBAL.API_CANCELADA, error.message);
            }
        }
    }

    getIdfromSelectedItem(item){
        let items_ocupados = this.state.items_ocupados;
        let data =  items_ocupados.filter(item_ocupado =>{
            if((item_ocupado.sabor_id === item._id) && (item_ocupado.producto_id === this.state.producto_id)){
                return item_ocupado;
            }else{
                return false;
            }
        });
        return data.length > 0 ? data[0]._id : null 
    }

    agregarsabor(item){
        this.setState({isloading:true});
        if(!item.agregado){
            let parametros = {
                sabor_id: item._id,
                producto_id:this.state.producto_id
            }
            // return console.log(parametros)
            axios.post(`productos/relproductosaboresdulces`,  parametros, {cancelToken: this.signal.token })
            .then(response =>this.getRelacionSaborProducto(this.state.producto_id));
        }else{
            let id = this.getIdfromSelectedItem(item);
            axios.delete(`productos/deleterelacionproductos/${id}`)
            .then((result) =>  this.getRelacionSaborProducto(this.state.producto_id));
        }
    }


    render() {
        let catalogo =  this.state.catalogo ? this.state.catalogo : [];
        let style= {
            imgStyle:{
                width:'4rem'
            },
            title:{
                'fontSize':'1.4rem'
            }
        }
        return (
            <div className="row">
                {
                    (catalogo || catalogo.length > 0 ) ?
                    catalogo.map((item,key) => {
                        return <div key={key}  className="col-xl-3 col-md-4 mb-4">
                                <div className="card border-left-danger shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Sabor</div>
                                            <div className="h5 mb-0 font-weight-bold text-gray-800" style={style.title}>{item.nombre}</div>
                                            <button disabled={this.state.isloading} onClick={()=>{ this.agregarsabor(item) }} className={ !item.agregado ? "btn btn-success btn-sm mt-2": "btn btn-danger btn-sm mt-2"}>
                                                <FontAwesomeIcon icon={!item.agregado ? "plus": "minus"}></FontAwesomeIcon> { !item.agregado ? " Agregar": " Quitar"}
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <img alt="Sabor" className="img-fluid" src={item.img && item.img.length > 0 ? `${item.img[0].public_path}` : `${GLOBAL.PATH_URL}noimagen`} style={style.imgStyle} ></img>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    }): <div className="card mb-4 py-3 col-md-6 border-left-danger">
                            <div className="card-body"> Sin Sabores </div>
                        </div>
                    }
            </div>
        )
    }
}
