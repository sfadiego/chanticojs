import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
let GLOBAL = require("../Core/Constantes");

export default class CardProteinas extends Component {
    constructor(props) {
        super(props);
        this.state ={
            items_ocupados:[],
            producto_id: null,
            catalogo:null,
            isloading:false
        };
        
    }
    signal = axios.CancelToken.source();
    
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        this.cargaCatalogo();
    }

    componentDidUpdate(prevProps){
        if(this.state.producto_id !== prevProps.producto_id){
            this.setState({producto_id:prevProps.producto_id})
            this.getRelacionAderezoProducto(prevProps.producto_id);
        }
    }

    getRelacionAderezoProducto = async (producto_id) =>{
        
        await axios.get( `productos/proteina/${producto_id}`, { cancelToken: this.signal.token }).then((result) => {
            let response = result.data;
            this.setState({items_ocupados:response})

            let catalogo = this.state.catalogo;
            catalogo.map((item_catalogo,key) =>{
                let getIndex = response.findIndex(item => item.proteina_id === item_catalogo._id);                
                return this.setState(state =>{
                      let item_orig = state.catalogo[key];
                      let item = item_orig['agregado'] = (getIndex === -1) ? false : true;
                      return item
                })
            });
            this.setState({isloading:false});
        }).catch((err) => {
            console.log(err);
        });
    }

    getRelacionproteinaProducto = async (producto_id) =>{
        
        await axios.get( `productos/proteina/${producto_id}`, { cancelToken: this.signal.token }).then((result) => {
            let response = result.data;
            this.setState({items_ocupados:response})

            let catalogo = this.state.catalogo;
            catalogo.map((item_proteina,key) =>{
                let getIndex = response.findIndex(item => item.proteina_id === item_proteina._id);                
                return this.setState(state =>{
                      let item_orig = state.catalogo[key];
                      let item = item_orig['agregado'] = (getIndex === -1) ? false : true;
                      return item
                })
            });
            this.setState({isloading:false});
        }).catch((err) => {
            console.log(err);
        });
    }

    getIdfromSelectedItem(item){
        let items_ocupados = this.state.items_ocupados;
        let data =  items_ocupados.filter(item_ocupado =>{
            if((item_ocupado.proteina_id === item._id) && (item_ocupado.producto_id === this.state.producto_id)){
                return item_ocupado;
            }else{
                return false;
            }
        });
        return data.length > 0 ? data[0]._id : null 
    }

    agregaraProteina(item){
        this.setState({isloading:true});
        if(!item.agregado){
            let parametros = {
                proteina_id: item._id,
                producto_id:this.state.producto_id
            }
            
            axios.post(`productos/relproteina`,  parametros, {cancelToken: this.signal.token }).then(response =>{
              this.getRelacionproteinaProducto(this.state.producto_id);
              
            });
        }else{
            let id = this.getIdfromSelectedItem(item);
            axios.delete(`productos/relproteina/${id}`).then((result) => {
                this.getRelacionproteinaProducto(this.state.producto_id);
            });
        }
    }

    cargaCatalogo = async () => {
        try {
            let listado =  await axios.get( 'proteina', { cancelToken: this.signal.token });
            let response_catalogo = listado.data
            this.setState({ catalogo:response_catalogo });
        } catch (error) {
            if (axios.isCancel(error)) {
                console.log(`${GLOBAL.API_CANCEL_REQUEST}`,error.message);
            }
        }

    }

    render() {
        let catalogo =  this.state.catalogo ? this.state.catalogo : {};
        let style= {
            imgStyle:{
                width:'4rem'
            },
            title:{
                'fontSize':'1.4rem'
            }
        }
        return (
            <div className="row">
                {
                    (catalogo.length > 0) ?
                    catalogo.map((item,key) => {
                        return <div key={key}  className="col-xl-3 col-md-4 mb-4">
                                <div className="card border-left-danger shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Proteina</div>
                                            <div className="h5 mb-0 font-weight-bold text-gray-800" style={style.title}>{item.nombre}</div>
                                            <button disabled={this.state.isloading} onClick={()=>{ this.agregaraProteina(item) }} className={ !item.agregado ? "btn btn-success btn-sm mt-2": "btn btn-danger btn-sm mt-2"}>
                                                <FontAwesomeIcon icon={!item.agregado ? "plus": "minus"}></FontAwesomeIcon> { !item.agregado ? " Agregar": " Quitar"}
                                            </button>
                                        </div>
                                        <div className="col-auto">
                                            <img alt="Proteina" className="img-fluid" src={item.img.length > 0 ? `${item.img[0].public_path}` : `${GLOBAL.PATH_URL}noimagen` } style={style.imgStyle} ></img>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    }): <div className="card mb-4 col-md-6 py-3 border-left-danger">
                            <div className="card-body"> Sin Proteinas </div>
                        </div>
                    }
            </div>
        )
    }
}
