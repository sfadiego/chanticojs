import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';

export default class Categorias extends Component {
    state = {
        categorias: [],
        selectedCategoria: null
    };

    componentDidMount() {
        axios.get('categoria').then(response => {
            const objcategoria = response.data;
            this.setState({ categorias: objcategoria });
        })
    }

    seleccionaCategorias = (id) => {
        this.setState({ selectedCategoria: id })
        this.props.getParentData(id);
    }

    render() {
        const activo = <FontAwesomeIcon size="lg" icon="check"></FontAwesomeIcon>
        return (
            this.state.categorias.map((item, key) => {
                return <div onClick={() => this.seleccionaCategorias(item._id)} key={key} className="col-lg-3 mb-4">
                    <div className="card mb-4 py-3 border-bottom-primary">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-9">
                                    {item.categoria}
                                </div>
                                <div className="col-md-3">
                                    {this.state.selectedCategoria === item._id ? activo : ''}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            })
        )
    }
}
