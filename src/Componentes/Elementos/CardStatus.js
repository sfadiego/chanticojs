import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class CardStatus extends Component {
    render() {
        let classtype = "card text-white shadow bg-" + this.props.type;
        return (
            <div className="col-md-3 mb-4">
                <div className={classtype}>
                    <div className="card-body">
                        Identificador: { this.props.nombre } <FontAwesomeIcon icon="pizza-slice" />
                        <div className="text-white-50 small">{ this.props.status } </div>
                    </div>
                </div>
            </div>
      
        );
    }
}

export default CardStatus;