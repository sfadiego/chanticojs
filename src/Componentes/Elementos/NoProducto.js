import React, { Component } from 'react';
import imgLogo from '../../Resources/img/logo_chantico.png';
export default class NoProducto extends Component {
  render() {
    return (
        <div className="col-md-12 pt-2 text-center">
             <img className="img-fluid img-comanda-logo mb-5 mt-5" alt="imagen empresa" src={imgLogo} ></img>
            <div className="card mb-4 py-3 border-left-danger">
                <div className="card-body">
                    Seleccione una categoria para iniciar el pedido
                </div>
            </div>
        </div>
    );
  }
}
