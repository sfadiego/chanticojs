import React, { Component } from 'react';

export default class NoItems extends Component {
  render() {
    return (
        <div className="col-md-12">
            <div className="card mb-4 py-3 border-left-danger">
                <div className="card-body">
                    No hay elementos que mostrar
                </div>
            </div>
        </div>
    );
  }
}
