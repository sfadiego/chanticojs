import React, { Component } from 'react'
import IMG_DEFAULT from '../../Resources/img/logo_chantico_secundario.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as GLOBAL from '../Core/Constantes'
export default class Productos extends Component {
  
  agregarpedido(obj){
    let newKeys = new Date();
    let objectData = {
        precio:obj.precio,
        producto: obj.producto,
        key: obj._id,
        unique: newKeys.getMilliseconds(),
        cantidad:1
      };
    this.props.AddProductsHandler(objectData);
  }

  render() {
    const precio = (this.props.items.precio) ? this.props.items.precio : 0;
    const producto = this.props.items.producto;
    let img_background = Object.keys(this.props.items.img).length > 0 ? `${GLOBAL.PATH_URL}${this.props.items.img[0].public_path}` : IMG_DEFAULT;
    let styleBackground = {
      backgroundImage: 'url(' + img_background + ')'
    };
    
    const item = (  
        <div className="item">
          <div className="bg_img" style={styleBackground}></div>
          <div className="item-precio">$ {precio }</div>
          <div className="item_title row">
              <div className="col-md-10 col-10 col-sm-10">
                {producto}
              </div>
              <div className="col-md-2 col-2 col-sm-2">
                <FontAwesomeIcon color="#28a745" icon="plus-circle"></FontAwesomeIcon> 
              </div>
          </div>
        </div>);

    return (
      <div onClick={() => { this.agregarpedido(this.props.items) }} className="col-md-3 col-6 col-sm-6 py-3 pr-1 pl-0 tab-pane fade show active" role="tabpanel" >
        { item } 
      </div>
    )
  }
}
