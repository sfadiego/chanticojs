import React, { Component } from 'react';
import * as Functions from '../Core/Funcions';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class EditarxItem extends Component {
    constructor(props) {
        super(props);
        this.state ={
            index:null,
            cantidad:0,
            descuento:'',
            producto:null,
            precioOriginal:0,
            total:0,
            extra:0,
            subtotal:0
        }
    }
    componentDidMount(){
        this.loadData()
    }

    loadData =()=>{
        let item = this.props.item[0];
        let cantidad = item.cantidad;
        let descuento = item.descuento;
        let index = item.index;
        let producto = item.producto;
        let precioOriginal = item.precioUnitario
        let extra = Functions.calculaTotalExtrasxRow(item);
        let subtotal = (extra + precioOriginal) * cantidad;
        let total = Functions.calculaTotalConDescuento(subtotal,descuento);

        this.setState({cantidad,
                        index,
                        descuento:descuento,
                        producto,
                        precioOriginal:precioOriginal,
                        total:total,
                        subtotal:subtotal,
                        extra:extra
                    })
    }

    calcularDescuento(event){
        let value = event.target.value ==="" ? 0 : parseInt(event.target.value);
        if(value > 99){ return this.setState({descuento:99}); }
        let descuento = value <= 0 ? 0 : value;
        return this.setState({descuento: descuento})
    }

    modificarItem(){
        let descuento = this.state.descuento;
        let data = {
            index: this.state.index,
            cantidad: this.state.cantidad,
            descuento: descuento,
            producto: this.state.producto,
            precioOriginal: this.state.precioOriginal,
            total: this.state.total,
            subtotal:this.state.subtotal
        }
        return this.props.modificarItemPorcentaje(data);
    }


    
    render() {
        let total = Functions.calculaTotalConDescuento(this.state.total,this.state.descuento)
        return (
            <div className="row pt-3">
                  <div className="col-md-3"></div>
                  <div className="col-md-6">
                    <div className="text-center">
                        <div className="info-price-value">
                            <span>{this.state.producto}</span>
                        </div>
                    </div>  
                    <div className="text-left text-danger">
                        { (this.state.descuento) ? this.state.descuento : 0} %
                    </div>
                    <div className="text-left">
                        { this.state.precioOriginal} c/u
                    </div>
                    <div className="text-left">
                        Extras: ${ this.state.extra}
                    </div>
                    <div className="text-left">
                        Subtotal: $ { this.state.subtotal }
                    </div>
                    <div className="text-left">
                        Total: $ { isNaN(total) ? this.state.subtotal :  total}
                    </div>
                    <form className="mt-3">
                        <div className="form-group">
                            <label htmlFor="producto">Cantidad</label>
                            <input type="number" className="form-control" disabled value={this.state.cantidad} onChange={event => this.setState({cantidad:event.target.value}) }></input>
                        </div>
                        <div className="form-group">
                            <label htmlFor="producto">Descuento</label>
                            <input type="number" min="1" max="99" value={this.state.descuento} placeholder="%" className="form-control" onChange={event => this.calcularDescuento(event) } >
                            </input>
                        </div>
                    </form>
                    <div className="">
                        <button onClick={ ()=> this.modificarItem() }  className="btn btn-primary col-md-8 br-0 mt-3"> 
                            <FontAwesomeIcon icon="percentage"></FontAwesomeIcon> Agregar 
                        </button>
                        <button onClick={ this.props.closeViewPorcentaje }  className="btn btn-danger col-md-4 br-0 mt-3"> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon> Cancelar 
                        </button>
                    </div>
                  </div>
                  <div className="col-md-3"></div>
            </div>
        )
    }
}
