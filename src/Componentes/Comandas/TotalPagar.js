import React, { Component } from 'react'

export default class TotalPagar extends Component {
    render() {
        return (
            <div className="row ">
                <div className="col-md-6">
                    <div className="text-right">
                        <div className="info-price-label">Deuda total: </div>
                        <div className="info-price-value">
                            <span><small>$</small><span>{this.props.total}</span></span>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 box-border-totales">
                    <div className="text-left">
                        <div className="info-price-label">Cambio</div>
                        <div className="info-price-value text-danger">
                            <span><small>$</small><span>{this.props.cambio}</span></span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
