import React, { Component } from 'react'
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../ProductoExtra/Style.css';
let GLOBAL = require("../Core/Constantes");

export default class ExtrasProductos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible:props.isVisibleExtras,
            item_id:props.item_id,
            checkAvalibleExtras:null,
            isloading:false,
            bebidas:[],
            bebida_visible:false,
            proteinas:[],
            proteina_visible:false,
            sabores:[],
            sabores_visible:false,
            saboresdulces:[],
            saboresdulces_visible:false,
            aderezos:[],
            aderezos_visible:false,
        }
    }
    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    closeView(){
        this.setState({isVisible:false,item_id:null});
        this.props.closeView();
    }
    
    loadBebidas = async (producto_id)=>{
        let relacion = await axios.get( `productos/getrelacionproductotipobebidabyProducto/${producto_id}`, { cancelToken: this.signal.token });
        if(relacion){
            this.setState({bebidas:[]});
            let tipobebida =  await axios.get( 'tipobebida', { cancelToken: this.signal.token })
            this.setState({ bebidas:tipobebida.data });
            this.setState({bebida_visible:true });
        }
    }

    loadSabores = async (producto_id)=>{
        let relacion = await axios.get( `productos/getrelacionproductosaboresbyProducto/${producto_id}`, { cancelToken: this.signal.token })
        if(relacion){
            let sabores =  await axios.get( 'sabores', { cancelToken: this.signal.token });
            sabores.data.map((item_sabor)=>{
                let getIndex = relacion.data.findIndex(rel => rel.sabor_id === item_sabor._id);                
                if(getIndex !== -1){
                    return this.setState(prevState =>({
                        sabores:[...prevState.sabores,item_sabor]
                    }));
                }else{ return false; }
            });
            this.setState({sabores_visible:true });
        }
    }

    loadSaboresdulces = async (producto_id)=>{
        let relacion = await axios.get( `productos/getrelacionproductossaboresdulcesbyProducto/${producto_id}`, { cancelToken: this.signal.token })
        if(relacion){
            let saboresdulces =  await axios.get( 'saboresdulces', { cancelToken: this.signal.token });
            saboresdulces.data.map((item_sabor)=>{
                let getIndex = relacion.data.findIndex(rel => rel.sabor_id === item_sabor._id);                
                if(getIndex !== -1){
                    return this.setState(prevState =>({
                        saboresdulces:[...prevState.saboresdulces,item_sabor]
                    }));
                }else{ return false; }
            });
            this.setState({saboresdulces_visible:true });
        }
    }

    loadProteinas = async (producto_id)=>{
        let relacion = await axios.get( `productos/proteina/${producto_id}`, { cancelToken: this.signal.token })
        if(relacion){
            let proteinas =  await axios.get( 'proteina', { cancelToken: this.signal.token });
            proteinas.data.map((item_proteina)=>{
                let getIndex = relacion.data.findIndex(rel => rel.proteina_id === item_proteina._id);                
                if(getIndex !== -1){
                    return this.setState(prevState =>({
                        proteinas:[...prevState.proteinas,item_proteina]
                    }));
                }else{ return false; }
            });
            this.setState({proteina_visible:true });
        }
    }

    loadAderezos = async (producto_id) => {
        try {
            let relacion = await axios.get( `productos/getrelacionproductosaderezos/${producto_id}`, { cancelToken: this.signal.token });
            if(relacion){
                let aderezos =  await axios.get( 'aderezos', { cancelToken: this.signal.token });
                aderezos.data.map(aderezo =>{
                    let getIndex = relacion.data.findIndex(rel => rel.aderezo_id === aderezo._id);                
                    if(getIndex !== -1){
                        return this.setState(prevState =>({
                            aderezos:[...prevState.aderezos,aderezo]
                        }));
                    }else{ return false; }
                });
                this.setState({ aderezos_visible:true });
            }
        } catch (error) {
            if (axios.isCancel(error)) { console.log(GLOBAL.API_CANCEL_REQUEST, error.message); }
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.item_id !== this.props.item_id && this.props.isVisibleExtras){
            if(this.props.item_id != null){
                this.checkAvalibleExtras(this.props.item_id,this.props.checkAvalibleExtras);
            }
        }
    }

    checkAvalibleExtras = async(item_id,obj)=>{
        let data = obj;
        if(data.valido && data.aderezo){
            this.setState({aderezos:[]});
            this.loadAderezos(item_id);
        }else{
            this.setState({
                aderezos_visible:false
            });
        }
        if(data.valido && data.proteina){
            this.setState({proteinas:[]});
            this.loadProteinas(item_id);
        }else{
            this.setState({
                proteina_visible:false
            });
        }

        if(data.valido && data.sabores){
            this.setState({sabores:[]});
            this.loadSabores(this.props.item_id);
        }else{
            this.setState({
                sabores_visible:false
            });
        }

        if(data.valido && data.saboresdulces){
            this.setState({saboresdulces:[]});
            this.loadSaboresdulces(this.props.item_id);
        }else{
            this.setState({
                saboresdulces_visible:false
            });
        }

        if(data.valido && data.tipo){
            
            this.loadBebidas(this.props.item_id);
        }else{
            this.setState({
                bebida_visible:false
            });
        }
        this.setState({isVisible:data.valido});
    }

    agregarExtra(item,item_id){
        if(item_id === GLOBAL.TABLA_EXTRAS.BEBIDAS_ID){
            let index = this.state.bebidas.findIndex(items_actuales =>items_actuales._id === item._id);
            this.setState(state =>{
                let item_original = state.bebidas[index];
                item_original['agregado'] = !item_original["agregado"];
                return item_original
            });
        }
        if(item_id === GLOBAL.TABLA_EXTRAS.SABOR_ID){
            let index = this.state.sabores.findIndex(items_actuales =>items_actuales._id === item._id);
            this.setState(state =>{
                let item_original = state.sabores[index];
                item_original['agregado'] = !item_original["agregado"];
                return item_original
            });
        }

        if(item_id === GLOBAL.TABLA_EXTRAS.SABOR_DULCE_ID){
            let index = this.state.saboresdulces.findIndex(items_actuales =>items_actuales._id === item._id);
            this.setState(state =>{
                let item_original = state.saboresdulces[index];
                item_original['agregado'] = !item_original["agregado"];
                return item_original
            });
        }

        if(item_id === GLOBAL.TABLA_EXTRAS.ADEREZO_ID){
            let index = this.state.aderezos.findIndex(items_actuales =>items_actuales._id === item._id);
            this.setState(state =>{
                let item_original = state.aderezos[index];
                item_original['agregado'] = !item_original["agregado"];
                return item_original
            });
        }

        if(item_id === GLOBAL.TABLA_EXTRAS.PROTEINA_ID){
            let index = this.state.proteinas.findIndex(items_actuales =>items_actuales._id === item._id);
            this.setState(state =>{
                let item_original = state.proteinas[index];
                item_original['agregado'] = !item_original["agregado"];
                return item_original
            });
        }
    }

    getSelectedItems(obj){
        return obj.filter(item =>  item.agregado === true);
    }

    confirmExtras(){ 
        let finalArray = [];
        let selectedBebidas   = this.getSelectedItems(this.state.bebidas);
        let selectedAderezos  = this.getSelectedItems(this.state.aderezos);
        let selectedSabores   = this.getSelectedItems(this.state.sabores);
        let selectedSaboresDulces   = this.getSelectedItems(this.state.saboresdulces);
        let selectedProteinas = this.getSelectedItems(this.state.proteinas);
        
        if(selectedBebidas.length > 0 ){
            Array.prototype.push.apply(finalArray,selectedBebidas);
        }
        if(selectedAderezos.length > 0 ){
            Array.prototype.push.apply(finalArray,selectedAderezos);
        }
        if(selectedSabores.length > 0 ){
            Array.prototype.push.apply(finalArray,selectedSabores);
        }

        if(selectedSaboresDulces.length > 0 ){
            Array.prototype.push.apply(finalArray,selectedSaboresDulces);
        }

        if(selectedProteinas.length > 0 ){
            Array.prototype.push.apply(finalArray,selectedProteinas);
        }
        
        return this.props.handleExtra(finalArray,this.props.itemSelected.producto.isToEdit, this.props.itemSelected.producto.unique);
    }

    render() {
        let aderezos = this.state.aderezos;
        let sabores = this.state.sabores;
        let proteinas = this.state.proteinas;
        let tipobebida = this.state.bebidas;
        let saboresdulces = this.state.saboresdulces;
        return (
            <div className={this.state.isVisible ?  "container_extras" : "container_extras d-none"}>
                <div className="row mb-3">
                    <div className="col-md-8"></div>
                    <div className="col-md-4 text-right pr-2">
                        <span className="btn btn-danger btn-sm col-md-3" onClick={()=>{ this.props.closeView(); }}> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>
                        </span>
                    </div>
                </div> 
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    {
                        (this.state.aderezos_visible) ? 
                        <li className="nav-item">
                            <a className="nav-link" id="pills-aderezo-tab" data-toggle="pill" href="#aderezo-tab" role="tab" aria-controls="aderezo-tab" aria-selected="true">Aderezos</a>
                        </li>:null
                    }
                    {
                        (this.state.sabores_visible) ? 
                        <li className="nav-item">
                            <a className="nav-link" id="pills-sabores-tab" data-toggle="pill" href="#pills-sabores" role="tab" aria-controls="pills-sabores" aria-selected="false">Sabores</a>
                        </li>:null
                    }
                    {
                        (this.state.proteina_visible) ?
                        <li className="nav-item">
                            <a className="nav-link" id="pills-proteinas-tab" data-toggle="pill" href="#pills-proteinas" role="tab" aria-controls="pills-proteinas" aria-selected="false">Proteinas</a>
                        </li> :null
                    }
                    {
                        (this.state.bebida_visible) ?
                        <li className="nav-item">
                            <a className="nav-link" id="pills-bebidas-tab" data-toggle="pill" href="#pills-bebidas" role="tab" aria-controls="pills-bebidas" aria-selected="false">Bebidas</a>
                        </li> :null
                    }
                    {
                        (this.state.saboresdulces_visible) ?
                        <li className="nav-item">
                            <a className="nav-link" id="pills-bebidas-tab" data-toggle="pill" href="#pills-sabores-dulces" role="tab" aria-controls="pills-sabores-dulces" aria-selected="false">Sabores dulces</a>
                        </li> :null
                    }
                </ul>
                <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="aderezo-tab" role="tabpanel" aria-labelledby="pills-aderezo-tab">
                        <div className="row options_aderezos">
                            {(this.state.aderezos_visible) ? 
                                aderezos.map((item,key) => {
                                    return <div key={key}  className="col-xl-4 col-md-4 mb-4 ">
                                                <div className={ item.agregado ? "card card-extras-selected shadow h-100 py-2 cursor-active" : "card shadow h-100 py-2 cursor-active"}>
                                                    <div className="card-body" onClick={()=>{ this.agregarExtra(item, GLOBAL.TABLA_EXTRAS.ADEREZO_ID) }}>
                                                        <div className="row no-gutters align-items-center">
                                                        <div className="col col-xs-10 col-md-10">
                                                            <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Aderezo</div>
                                                        </div>
                                                        <div className="col-xs-2 col-md-2 text-right">
                                                            {
                                                                item.agregado ? <FontAwesomeIcon className="selected-icon" icon="check"></FontAwesomeIcon> : null
                                                            }
                                                        </div>
                                                        <div className="col col-xs-8 col-md-8">
                                                            <div className="h5 mb-0 font-weight-bold text-gray-800">{item.nombre}</div>
                                                        </div>
                                                        <div className="col col-xs-4 col-md-4">
                                                            <div className="text-gray-600 "> ${item.precio } <FontAwesomeIcon icon="tag" color="#f0bf18"></FontAwesomeIcon> 
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                            }) : null}
                        </div>
                    </div>
                    <div className="tab-pane fade"  id="pills-sabores" role="tabpanel" aria-labelledby="pills-sabores-tab">
                        <div className="row options_sabores">
                            {(this.state.sabores_visible) ? 
                                sabores.map((item,key) => {
                                    return <div key={key}  className="col-xl-4 col-md-4 mb-4">
                                            <div className={ item.agregado ? "card card-extras-selected shadow h-100 py-2 cursor-active" : "card shadow h-100 py-2 cursor-active"}>
                                                <div className="card-body" onClick={()=>{ this.agregarExtra(item, GLOBAL.TABLA_EXTRAS.SABOR_ID) }}>
                                                    <div className="row no-gutters align-items-center">
                                                        <div className="col col-xs-10 col-md-10">
                                                            <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Sabores</div>
                                                        </div>
                                                        <div className="col-xs-2 col-md-2 text-right">
                                                            {
                                                                item.agregado ? <FontAwesomeIcon className="selected-icon" icon="check"></FontAwesomeIcon> : null
                                                            }
                                                        </div>
                                                        <div className="col col-xs-8 col-md-8">
                                                            <div className="h5 mb-0 font-weight-bold text-gray-800">{item.nombre}</div>
                                                        </div>
                                                        <div className="col col-xs-4 col-md-4">
                                                            <div className="text-gray-600 "> ${item.precio } <FontAwesomeIcon icon="tag" color="#f0bf18"></FontAwesomeIcon> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                }) : null}
                        </div>
                    </div>
                    <div className="tab-pane fade"  id="pills-sabores-dulces" role="tabpanel" aria-labelledby="pills-sabores-dulces-tab">
                        <div className="row options_sabores_dulces">
                            {(this.state.saboresdulces_visible) ? 
                                saboresdulces.map((item,key) => {
                                    return <div key={key}  className="col-xl-4 col-md-4 mb-4">
                                            <div className={ item.agregado ? "card card-extras-selected shadow h-100 py-2 cursor-active" : "card shadow h-100 py-2 cursor-active"}>
                                                <div className="card-body" onClick={()=>{ this.agregarExtra(item, GLOBAL.TABLA_EXTRAS.SABOR_DULCE_ID) }}>
                                                    <div className="row no-gutters align-items-center">
                                                        <div className="col col-xs-10 col-md-10">
                                                            <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Sabores dulces</div>
                                                        </div>
                                                        <div className="col-xs-2 col-md-2 text-right">
                                                            {
                                                                item.agregado ? <FontAwesomeIcon className="selected-icon" icon="check"></FontAwesomeIcon> : null
                                                            }
                                                        </div>
                                                        <div className="col col-xs-8 col-md-8">
                                                            <div className="h5 mb-0 font-weight-bold text-gray-800">{item.nombre}</div>
                                                        </div>
                                                        <div className="col col-xs-4 col-md-4">
                                                            <div className="text-gray-600 "> ${item.precio } <FontAwesomeIcon icon="tag" color="#f0bf18"></FontAwesomeIcon> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                }) : null}
                        </div>
                    </div>
                    <div className="tab-pane fade" id="pills-proteinas" role="tabpanel" aria-labelledby="pills-proteinas-tab">
                        <div className="row options_proteina">
                        {(this.state.proteina_visible) ? 
                            proteinas.map((item,key) => {
                                return <div key={key}  className="col-xl-4 col-md-4 mb-4 ">
                                        <div className={ item.agregado ? "card card-extras-selected shadow h-100 py-2 cursor-active" : "card shadow h-100 py-2 cursor-active"}>
                                            <div className="card-body" onClick={()=>{ this.agregarExtra(item, GLOBAL.TABLA_EXTRAS.PROTEINA_ID) }}>
                                                <div className="row no-gutters align-items-center">
                                                <div className="col col-xs-10 col-md-10">
                                                    <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Proteina</div>
                                                </div>
                                                <div className="col-xs-2 col-md-2 text-right">
                                                    {
                                                        item.agregado ? <FontAwesomeIcon className="selected-icon" icon="check"></FontAwesomeIcon> : null
                                                    }
                                                </div>
                                                <div className="col col-xs-8 col-md-8">
                                                    <div className="h5 mb-0 font-weight-bold text-gray-800">{item.nombre}</div>
                                                </div>
                                                <div className="col col-xs-4 col-md-4">
                                                    <div className="text-gray-600 "> ${item.precio } <FontAwesomeIcon icon="tag" color="#f0bf18"></FontAwesomeIcon> 
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            }) : null}
                        </div>
                    </div> 
                    <div className="tab-pane fade" id="pills-bebidas" role="tabpanel" aria-labelledby="pills-bebidas-tab">
                        <div className="row options_bebidas">
                            {(this.state.bebida_visible) ? 
                                tipobebida.map((item,key) => {
                                    return <div key={key}  className="col-xl-4 col-md-4 mb-4 ">
                                            <div className={ item.agregado ? "card card-extras-selected shadow h-100 py-2 cursor-active" : "card shadow h-100 py-2 cursor-active"}>
                                                <div className="card-body" onClick={()=>{ this.agregarExtra(item, GLOBAL.TABLA_EXTRAS.BEBIDAS_ID) }}>
                                                    <div className="row no-gutters align-items-center">
                                                    <div className="col col-xs-10 col-md-10">
                                                        <div className="text-xs font-weight-bold text-success text-uppercase mb-1">Bebida</div>
                                                    </div>
                                                    <div className="col-xs-2 col-md-2 text-right">
                                                        {
                                                            item.agregado ? <FontAwesomeIcon className="selected-icon" icon="check"></FontAwesomeIcon> : null
                                                        }
                                                    </div>
                                                    <div className="col col-xs-8 col-md-8">
                                                        <div className="h5 mb-0 font-weight-bold text-gray-800">{item.nombre}</div>
                                                    </div>
                                                    <div className="col col-xs-4 col-md-4">
                                                        <div className="text-gray-600 "> ${item.precio } <FontAwesomeIcon icon="tag" color="#f0bf18"></FontAwesomeIcon> 
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                }) 
                                : null } 
                        </div>
                    </div> 
                    
                    <div className="row">
                        <div className="col-md-3">
                            <span className="btn btn-primary btn-sm btn-block" onClick={()=>{ this.confirmExtras(); }}> 
                                Agregar
                            </span>
                        </div>
                        <div className="col-md-9"></div>
                    </div>
                </div>
            </div>
        )
    }
}
