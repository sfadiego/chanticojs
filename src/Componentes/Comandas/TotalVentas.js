import React from 'react';

let TotalVentas = (props) =>
    <div className="row">
        <div className="col-md-12 text-center">
            <h2 className="text-primary">Venta del dia: ${props.totalVentas}</h2>
        </div>
        <div className="col-md-6 offset-md-3 mt-3">
            <ul className="list-group">
                {
                    props.ventas.map((item, key) => {
                        return <li className="list-group-item d-flex justify-content-between align-items-center" key={key}>
                            {item.producto}
                            <span className="badge badge-primary badge-pill">
                                {item.cantidad}
                            </span>
                        </li>
                    })
                }

            </ul>
        </div>
    </div>

export default TotalVentas;