import React, { Component } from 'react'
import axios from 'axios';
import DataTable from 'react-data-table-component';
import {Link} from 'react-router-dom';
import LoadingComponent from '../Elementos/LoadingComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Funciones from '../Core/Funcions'
import * as TablaTerminados from './TablaTerminados';
let GLOBAL = require("../Core/Constantes");

export default class DetalleTerminados extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columnas:[],
            data:[],
            isLoading:false,
            pedidoInfo:[],
            total:0,
            subtotal:0,
            descuento:0
        }
    }
    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);
    componentDidMount(){
        this.loadComponentInfo();
    }

    loadComponentInfo = async()=>{
        try {
            const comandaid = this.props.match.params.id;
            let relComandaPedido = await axios.get('comandaByPedidoId/' + comandaid,{ cancelToken: this.signal.token });
            let comanda = relComandaPedido.data.data.comanda;
            await this.getExtras(comanda);
            let pedido = relComandaPedido.data.data.pedido;
            if(!pedido){ return this.props.history.push("/mesas/"); }
            let pedidoInfo = await Funciones.getDataAxios(`pedidos/${comandaid}`);
            let columnas = TablaTerminados.tablaComanda();
            columnas.splice(1,0,{
                name: 'Producto',
                sortable: true,
                cell: row => {
                    let htmlextra = []
                    if(row.extras && row.extras.length > 0){
                        let extras = row.extras;
                        extras.map((item, key)=>{
                            return htmlextra.push(
                                <div key={key} className="row ml-2">
                                    <div className="col-12 col-sm-12 extras-sm text-xs" > { item.nombre } ${item.precio}  </div>
                                </div>
                            );
                        })
                    }
                    
                    return <div> 
                        <div>{row.producto} </div>
                        {htmlextra}
                    </div>
                    
                }
            });
            
            this.setState({
                pedidoInfo:pedidoInfo,
                data: comanda,
                columnas:columnas
            },()=>this.calculaTotal());
        } catch (error) {
            if (axios.isCancel(error)) { console.log(GLOBAL.API_CANCELADA, error.message); } 
        }
    }

    getExtras = async (data)=> {
        const comandaid = this.props.match.params.id;
        let extras = await Funciones.getDataAxios(`comanda/extras/${comandaid}`);
        let productos = data;
        productos.map(producto =>{
          let obj_Extra = extras.filter(item => (item.key === producto.key && item.unique === producto.unique))
          return producto.extras = [].concat(obj_Extra);
        });      
      }

    calculaTotal = async(obj) =>{
        if(this.state.data.length === 0){
            return this.setState({total:0, subtotal:0, descuento:0});
        }

        let productos = this.state.data;
        let total = 0;
        productos.map(async element => {
            let total_extras = await Funciones.calculaExtras(element.extras).then((data)=> { return data; }, (error)=> error);
            let calculaTotal = Funciones.calculaTotal(element,total,total_extras)
            total = calculaTotal.total
            if (this.state.descuento) {
                return this.setState({
                    total: Funciones.calculaTotalConDescuento(calculaTotal.subtotal,this.state.descuento),
                    subtotal: calculaTotal.subtotal,
                    descuento: parseInt(this.state.descuento)
                });
            }

            return this.setState({ total: calculaTotal.total,  subtotal: calculaTotal.subtotal });
        });
    }


    render() {
        let columnas = this.state.columnas;
        let data = this.state.data;
        let fecha_pedido = Funciones.formatDate(this.state.pedidoInfo.fecha_pedido);
        return (
            <div className = "container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Orden </h1>
                </div>
                <div className="row">
                    <div className="col-md-10"></div>
                    <div className="col-md-2 text-right mt-2">
                        <span className="btn btn-danger br-0" onClick={()=>{ 
                            this.props.history.push("/comandas/terminadas/") }}> 
                            <FontAwesomeIcon icon="arrow-left"></FontAwesomeIcon> Regresar
                        </span>
                    </div>
                </div>
                {
                    !this.state.isLoading ? 
                    <div className="row row-container">
                        <div className="col-md-3">
                                <div style={{minHeight:'60vh'}}>
                                    <div className="info_column">
                                        <small className="info_label" >Tienda</small>
                                        <div className="info_value">{GLOBAL.STORE_NAME}</div>
                                    </div>
                                    <div className="info_column">
                                        <small className="info_label" >Orden ID</small>
                                        <div className="info_value">{this.state.pedidoInfo._id}</div>
                                    </div>
                                    <div className="info_column">
                                        <small className="info_label" >Nombre de Orden</small>
                                        <div className="info_value">{this.state.pedidoInfo.nombre_pedido}</div>
                                    </div>
                                    <div className="info_column">
                                        <small className="info_label" >Empleado</small>
                                        <div className="info_value" >Gabriela Zepeda</div>
                                    </div>
                                    <div className="info_column">
                                        <small className="info_label">Fecha pedido</small>
                                        <div className="info_value" >{fecha_pedido}</div>
                                    </div>
                                </div>
                                <hr className="hr-block-info"></hr>
                                <div className="text-xs">
                                    <div className="container-totales">
                                        <div className="container-item">Descuento: </div>
                                        <div className="container-item text-right">
                                            {this.state.descuento}%
                                        </div>
                                    </div>
                                    <div className="container-totales">
                                        <div className="container-item">Subtotal: </div>
                                        <div className="container-item text-right">
                                            ${this.state.subtotal}
                                        </div>
                                    </div>
                                    <div className="container-totales">
                                        <div className="container-item">Total: </div>
                                        <div className="container-item text-right">
                                            ${this.state.total}
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div className="col-md-9 right-container">
                            <DataTable
                                columns={columnas}
                                pagination
                                data={data} />
                            <div className="col-md-12 text-center info-footer-container">
                                <Link className="btn btn-success col-md-3 br-0" to={`/imprimir/ticket/${this.state.pedidoInfo._id}`}> 
                                    <FontAwesomeIcon icon="clipboard"></FontAwesomeIcon> Ticket 
                                </Link>
                            </div>
                        </div>
                    </div>
                    :<LoadingComponent></LoadingComponent>
                }
            </div>
        )
    }
}
