import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../Componentes/Core/Store/actions/index'
import LoadingComponent from '../Elementos/LoadingComponent';
import Axios from 'axios';
import VentasInfo from './VentasInfo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TotalVentas from './TotalVentas';
import * as GLOBAL from '../Core/Constantes'
import Logo from '../../Resources/img/logo_chantico_secundario.png'
import Aux from '../Layout/AuxComponent'
class Cerrarcaja extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ventas: [],
            totalVendido: 0,
            error: null
        }
        this.cerraCaja = this.cerraCaja.bind(this);
        this.abrirVentas = this.abrirVentas.bind(this);
    }

    cerraCaja() {
        this.props.cerrar(this.props.tiendaNombre)
        this.totalVendido();
        this.ventaxDia();
    }

    componentDidMount() {
        if (this.props.cerrada) {
            this.totalVendido();
            this.ventaxDia();
        }
    }

    abrirVentas() {
        this.props.abrirCaja(this.props.tiendaNombre)
    }

    totalVendido() {
        Axios.get('/totalventadiaria/')
            .then(success => this.setState({ totalVendido: success.data.ventaTotal }))
            .catch(error => this.setState({ error: error }));
    }

    ventaxDia() {
        Axios.post('/resumen/ventas/')
            .then(success => this.setState({ ventas: success.data }))
            .catch(error => this.setState({ error: error }));
    }

    render() {
        let component = <VentasInfo
            cerrada={this.props.cerrada}
            cerraCaja={this.cerraCaja}
            cerradoHasta={this.props.cerradaHasta} />

        let showVentasDetail = null;
        if (this.props.cerrada) {
            showVentasDetail = <TotalVentas totalVentas={this.state.totalVendido} ventas={this.state.ventas}></TotalVentas>
        }
        if (this.props.isLoading) {
            component = <LoadingComponent></LoadingComponent>
        }


        return (
            <div className="container-fluid pb-3">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <img className="img-fluid img-comanda-logo" alt="imagen empresa" src={Logo} ></img>
                    </div>
                </div>
                {this.props.role === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
                    <Aux>
                        {component}
                        {showVentasDetail}
                    </Aux>
                    :
                    <div className="row mt-3">
                        <div className="col-md-12 text-center">
                            <h1 className="text-primary">Bienvenido </h1>
                        </div>
                    </div>
                }

                {
                    this.props.role === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
                        <div className="row mb-3">
                            <div className="col-md-12 text-center mt-3">

                                {
                                    !this.props.cerrada ?
                                        <button onClick={this.cerraCaja} className="btn btn-danger col-md-3">
                                            <FontAwesomeIcon icon="shopping-cart"></FontAwesomeIcon> Corte de caja
                                        </button>
                                        : null
                                }

                                {
                                    this.props.cerrada ?
                                        <button onClick={this.abrirVentas} className="btn btn-primary col-md-3">
                                            <FontAwesomeIcon icon="money-bill-alt"></FontAwesomeIcon> Abrir
                                    </button>
                                        : null
                                }
                            </div>
                        </div> : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        tiendaNombre: state.auth.tienda,
        cerrada: state.auth.cajaCerrada,
        isLoading: state.auth.loading,
        cerradaHasta: state.auth.cerradaHasta,
        role: parseInt(state.auth.role)
    }
}
const mapDispatchToProps = dispatch => {
    return {
        cerrar: (tiendaNombre) => dispatch(actions.closeSales(tiendaNombre)),
        abrirCaja: (tienda) => dispatch(actions.openSales(tienda))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cerrarcaja);