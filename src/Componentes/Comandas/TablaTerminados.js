import * as Funciones from '../Core/Funcions'

export function columnas(){
    return [
        {
            name: 'Pedido',
            sortable: true,
            cell: row => `${row._id.substr(row._id.length - 5).toUpperCase()}`
        },
        {
            name: 'Nombre de orden',
            selector: 'nombre_pedido',
            sortable: true,
            center: true,
        },
        {
            name: 'Total',
            sortable: true,
            center: true,
            cell: row =>{
                return `$ ${row.total}`
            }
        },
        {
            name: 'Subtotal',
            sortable: true,
            center: true,
            cell: row =>{
                return `$ ${row.subtotal}`
            }
        },
        {
            name: 'Descuento',
            sortable: true,
            center: true,
            cell: row =>{
                return `${row.descuento}%`
            }
        },
        {
            name: 'Creado',
            sortable: true,
            center: true,
            cell: row=> Funciones.formatDate(row.createdAt)
        },
    ]; 
}

export function tablaComanda() {
    return [
        {
            name: '#',
            sortable: true,
            center:true,
            cell: row => `${row._id.substr(row._id.length - 5).toUpperCase()}`
        },
        
        {
            name: 'Precio',
            sortable: true,
            center:true,
            cell: row => `$${row.precio}`
        },
        {
            name: 'Cantidad',
            sortable: true,
            center:true,
            cell: row => row.cantidad
        },
        {
            name: 'Ingredientes extras',
            sortable: true,
            center:true,
            cell: row => `$${Funciones.calculaTotalExtrasxRow(row)}`
        },
        {
            name: 'Descuento',
            sortable: true,
            center:true,
            cell: row => `${row.descuento}%`
        },
        {
            name: 'Total',
            sortable: true,
            center:true,
            cell: row =>   `$${Funciones.calculaExtrasYproductoxRow(row)}`
        },

    ];
}
