import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class AgregaDescuento extends Component {
    constructor(props) {
        super(props);
        this.state = {
            descuento:0,
        }
    }

    calcularDescuento(event){
        let value = event.target.value ==="" ? 0 : parseInt(event.target.value);
        if(value > 99){ return this.setState({descuento:99}); }
        let descuento = value <= 0 ? 0 : value;
        return this.setState({descuento: descuento})
    }

    agregarDescuento(){
        let descuento = this.state.descuento
        this.props.calculaTotalDescuento(descuento);
    }

    render() {
        return (
            <div className="row pt-3">
                  <div className="col-md-3"></div>
                  <div className="col-md-6">
                    <div className="">
                        <div className="info-price-value">
                            <span>Descuento: </span> 
                            <span className="text-danger">
                                { (this.state.descuento) ? this.state.descuento : 0} %
                            </span>
                        </div>
                    </div>  
                    <form className="mt-3">
                        <div className="form-group">
                            <label htmlFor="producto">Descuento</label>
                            <input type="number" min="1" max="99" value={this.state.descuento} placeholder="%" className="form-control" onChange={event => this.calcularDescuento(event) } >
                            </input>
                        </div>
                    </form>
                    <div className="">
                        <button onClick={ ()=> this.agregarDescuento() }  className="btn btn-primary col-md-8 br-0 mt-3"> 
                            <FontAwesomeIcon icon="percentage"></FontAwesomeIcon> Agregar 
                        </button>
                        <button onClick={ this.props.closeViewPorcentaje }  className="btn btn-danger col-md-4 br-0 mt-3"> 
                            <FontAwesomeIcon icon="backspace"></FontAwesomeIcon> Cancelar 
                        </button>
                    </div>
                  </div>
                  <div className="col-md-3"></div>
            </div>
        );
    }
}

export default AgregaDescuento;