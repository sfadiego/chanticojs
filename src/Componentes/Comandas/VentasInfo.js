import React from 'react'
import * as funciones from '../../Componentes/Core/Funcions'

const VentasInfo = (props) => {
    return (
        <div className="row">
            
            <div className="col-md-12 text-center mt-2">
                {props.cerrada
                    ?
                    <div>
                        <h1 className="text-danger">Fin de ventas</h1>
                        <h3 className="text-danger">{` ${funciones.formatDate()}`}</h3>
                        <h4>
                            <span className="text-primary">  Se abre:  </span>  <span className="text-danger"> {` ${funciones.formatDate(props.cerradoHasta)}`} </span>
                        </h4>
                    </div>
                    :
                    <h1 className="text-primary">Bienvenido </h1>
                }
            </div>
        </div>
    )
}

export default VentasInfo
