import React, { Component } from 'react'
import DataTable from 'react-data-table-component';
import axios from 'axios';
import * as TablePedidosConfig from '../Tablas/TablaComandaConfiguracions';
import * as Functions from '../Core/Funcions';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoItems from '../Elementos/NoItems';
import LoadingComponent from '../Elementos/LoadingComponent'
import Aux from '../Layout/AuxComponent';
import withErrorHandler from '../../ErrorBoundary/ErrorHandler';
let GLOBAL = require("../Core/Constantes");

class ListaPedidos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            columns: [],
            status: [],
            catalogoEstatus: [],
            isShowPedidoDetail: false,
            isShowTable: true,
            nombrePedido: null,
            showSpinner: false,
            errormsg: null
        }
        this.confirmarPedido = this.confirmarPedido.bind(this);
    }
    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA} categorias`);

    componentDidMount() {
        this.cargarListado();
    }
    cargarListado = async () => {
        let estatus = await Functions.getDataAxios('catestatuspedido', this.signal.token);
        let status_ocupado = Functions.getStatusOcupado(estatus);
        let status_proceso = Functions.getStatusProceso(estatus);
        let columnas = TablePedidosConfig.createColumnsComandaActiva();
        columnas.splice(2, 0, {
            name: 'Estatus',
            sortable: true,
            center: true,
            button: true,
            cell: row => {
                if (row.estatus_id[0]._id === status_proceso._id) {
                    return <span className="badge badge-success">{row.estatus_id[0].estatus}</span>
                } else if (row.estatus_id[0]._id === status_ocupado._id) {
                    return <span className="badge badge-warning">{row.estatus_id[0].estatus}</span>
                }
            }
        });

        columnas.push({
            name: '#',
            sortable: true,
            right: true,
            cell: row => {
                let id = `/comanda/${row._id}`
                return <Link className="btn btn-primary" to={id}>  <FontAwesomeIcon icon="utensils"></FontAwesomeIcon> </Link>
            }
        })

        columnas.push({
            name: '#',
            sortable: true,
            center: true,
            cell: row => {
                let ocupado = (row.estatus_id[0]._id === status_ocupado._id) ? true : false;
                if (ocupado) {
                    return <Button className="btn btn-danger" onClick={() => this.props.history.push(`/imprimir/ticket/${row._id}`)}>
                        <FontAwesomeIcon icon="print"></FontAwesomeIcon>
                    </Button>
                } else if (row.total > 0) {
                    return <Button className="btn btn-success" onClick={() => {
                        axios({
                            method: GLOBAL.PUT,
                            url: `pedidos/ocupado/${row._id}`,
                            data: { estatus_id: status_ocupado._id }
                        }).then(success => this.props.history.push(`/imprimir/ticket/${row._id}`));

                    }}>  <FontAwesomeIcon icon="print"></FontAwesomeIcon> </Button>
                }
                return <span className=""> -- </span>
            }
        })

        await Functions.getDataAxios(`pedidosActivos`, this.signal.token)
            .then(succes => this.setState({ data: succes, columns: columnas }))
            .catch(error => this.setState({ errormsg: GLOBAL.UNAVAILABLE_CONTENT }));

        await Functions.getDataAxios('catestatuspedido', this.signal.token)
            .then(success => this.setState({ status: success }))
            .catch(error => this.setState({ errormsg: GLOBAL.UNAVAILABLE_CONTENT }));
    }

    AbrirPedido() {
        return this.setState({ isShowPedidoDetail: true, isShowTable: false });
    }

    confirmarPedido(event) {
        event.preventDefault();
        let status = Functions.getStatusProceso(this.state.status);
        let nombre_pedido = this.state.nombrePedido ? this.state.nombrePedido : Functions.generaFolioPedido();
        let data = {
            "estatus_id": status._id,
            "nombre_pedido": nombre_pedido
        }
        this.setState({ showSpinner: true })
        axios.post('pedidos', data)
            .then(response => this.props.history.push(`/comanda/${response.data._id}`))
            .catch(error => this.setState({ errormsg: GLOBAL.UNAVAILABLE_CONTENT }));
    }

    render() {
        let data = this.state.data;
        let columns = this.state.columns;
        let component = null;

        if (this.state.isShowTable) {
            component = <Aux>
                <div className="offset-md-10 text-right">
                    <span className="btn btn-success" onClick={() => { this.AbrirPedido(); }}>
                        <FontAwesomeIcon icon="clipboard"></FontAwesomeIcon> Abrir mesa
                    </span>
                </div>
                <div className="col-md-12">
                    {
                        Object.keys(this.state.data).length > 0 ?
                            <DataTable
                                columns={columns}
                                data={data} />
                            : <NoItems />
                    }

                </div>
            </Aux>
        }

        if (this.state.showSpinner) {
            component = (
                <Aux>
                    <LoadingComponent></LoadingComponent>
                </Aux>
            )
        }

        if (this.state.isShowPedidoDetail) {
            component = <Aux>
                <div className="offset-md-10 text-right">
                    <span className="btn btn-danger" onClick={() => this.setState({ isShowTable: true, isShowPedidoDetail: false })}>
                        <FontAwesomeIcon icon="backspace"></FontAwesomeIcon>
                    </span>
                </div>
                <form className="offset-md-3 col-md-6" onSubmit={this.confirmarPedido}>
                    <div className="form-group">
                        <label htmlFor="producto">Nombre de comanda</label>
                        <input autoFocus type="text" className="form-control" onChange={event => this.setState({ nombrePedido: event.target.value })}></input>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary col-md-3 btn-sm">Continuar</button>
                    </div>
                </form>
            </Aux>
        }

        return (
            <div className="container-fluid">
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Mesas</h1>
                </div>
                <div className="row">
                    {component}
                </div>
            </div>
        )
    }
}

export default withErrorHandler(ListaPedidos, axios);