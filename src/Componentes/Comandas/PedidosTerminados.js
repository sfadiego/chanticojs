import React, { Component } from 'react'
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import LoadingComponent from '../Elementos/LoadingComponent';
import * as TablaTerminados from './TablaTerminados';
import { Link } from 'react-router-dom';

let GLOBAL = require("../Core/Constantes");

export default class PedidosTerminados extends Component {
    constructor(props) {
        super(props)
        this.state = {
            columnas: [],
            data: [],
            isLoading: true,
            error: null,
            fechaBusqueda: null,
        }
        this.handleSearch = this.handleSearch.bind(this);
    }

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`${GLOBAL.API_CANCELADA}`);

    componentDidMount() {
        try {
            this.loadData();
        } catch (error) {
            if (axios.isCancel(error)) return console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
        }
    }

    // Validar por fechas
    loadData = async () => {
        try {
            axios.get(`pedidos-terminados`, { cancelToken: this.signal.token }).then(response => {
                let columnas = TablaTerminados.columnas();
                columnas.push({
                    name: '--',
                    sortable: true,
                    center: true,
                    cell: row => {
                        let url = `/revisar/comanda/${row._id}`;
                        return <Link className="btn btn-success" to={url}>
                            <FontAwesomeIcon icon="list"></FontAwesomeIcon>
                        </Link>
                    }
                });
                this.setState({ data: response.data, columnas: columnas, isLoading: false });
            }).catch(error => this.setState({ error: GLOBAL.UNAVAILABLE_CONTENT }))

        } catch (error) {
            if (axios.isCancel(error)) return console.log(GLOBAL.API_CANCEL_REQUEST, error.message);
            console.log(error);
        }
    }

    regresar() {
        return this.props.history.push('/comandas/');
    }

    handleSearch(event) {
        event.preventDefault();
        this.setState({ isLoading: true });
        this.searchByDate()
            .then(success => this.setState({ data: success.data, isLoading: false }))
            .catch(error => console.log(error));
    }

    searchByDate = async () => {
        return new Promise((resolve, reject) => {
            let fecha = this.state.fechaBusqueda;
            axios({
                method: GLOBAL.POST,
                url: `pedidos-terminados-by-fecha/`,
                data: { fecha: fecha },
            }, { cancelToken: this.signal.token })
                .then(success => resolve(success))
                .catch(error => reject(error));
        });

    }

    render() {
        let columnas = this.state.columnas;
        let data = this.state.data
        let component = null;

        component = <div className="row">
            <div className="col-md-12">
                <form className="row" onSubmit={this.handleSearch}>
                    <div className="form-group col-md-2">
                        <input type="date" onChange={event => this.setState({ fechaBusqueda: event.target.value })} className="form-control"></input>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            <FontAwesomeIcon icon="search"></FontAwesomeIcon>
                        </button>
                    </div>
                </form>
            </div>
            <div className="col-md-12">
                <DataTable columns={columnas} pagination data={data} />
            </div>
        </div>


        if (this.state.isLoading) {
            component = <LoadingComponent></LoadingComponent>;
        }

        return (
            <div className="container-fluid" >
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-gray-800">Ventas </h1>
                </div>
                {
                    this.state.error ?
                        <div className="row">
                            <div className="col-md-12 text-center">
                                <h2 className="text-danger">{this.state.error}</h2>
                            </div>
                        </div>
                        : null
                }
                {component}
            </div>
        )
    }
}
