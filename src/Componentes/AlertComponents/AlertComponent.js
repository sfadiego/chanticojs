import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import '../AlertComponents/Styles.css';
let GLOBAL = require("../Core/Constantes");

export default class AlertComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: GLOBAL.SUCCES,
            show: props.show,
            type: GLOBAL.MODAL_SUCCESS,
            titulo: (props.title) ? props.title : GLOBAL.DEFAULT_MODAL_TITLE,
            icono: props.icon || "check-circle",
            confirmButtonText: props.confirmButtonText || "Continuar",
            header_style: '',
            button: 'primary',
            modalAcceptInputTypeNumber: false,
            input_value: '',
        }
    }


    confirmEvent() {
        let value = this.state.input_value

        this.props.confirmEvent(value);
    }

    closeModal() {
        this.props.closeModal();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.show !== this.props.show) {
            let icono = null;
            let header = '';
            let button = '';
            if (this.props.type === GLOBAL.MODAL_SUCCESS) {
                icono = 'check-circle';
                header = 'modal-primary-header';
                button = 'primary';
            }

            if (this.props.type === GLOBAL.MODAL_ERROR) {
                icono = 'times';
                header = 'modal-danger-header';
                button = 'danger';
            }

            this.setState({
                icono: icono,
                show: this.props.show,
                header_style: header,
                type: this.props.type,
                text: this.props.text,
                button: button,
                modalAcceptInputTypeNumber: this.props.modalAcceptInputTypeNumber,
                input_value: this.props.input_value
            });
        }
    }


    render() {
        const titulo = this.state.titulo;
        let text = this.state.text;
        let confirm_button_text = this.state.confirmButtonText;
        let disabled = parseInt(this.state.input_value) > 99 ? true : false;
        return (
            <Modal backdrop="static" show={this.state.show} onHide={() => this.closeModal()}>
                <Modal.Header className={this.state.header_style}>
                    <Modal.Title>{titulo}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p className="mt-2">{text}</p>
                    {
                        this.state.modalAcceptInputTypeNumber ?
                            <div className="form-group">
                                <label htmlFor="color">Descuento <span style={{ color: "red" }}>*</span></label>
                                <input type="number" min="1" max="99" placeholder="%" className="form-control" onChange={event => {
                                    this.setState({
                                        input_value:
                                            event.target.value <= 0
                                                ? 0
                                                :
                                                event.target.value
                                    })
                                }} >
                                </input>
                            </div> : null
                    }

                    {
                        this.props.inputText ?
                            <div className="form-group">
                                <input type="text" className="form-control" onChange={event => this.setState({ input_value: event.target.value })} >
                                </input>
                            </div> : null
                    }

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="light btn-sm" onClick={() => this.closeModal()}>
                        Cerrar
                </Button>
                    <Button disabled={disabled} variant={`${this.state.button} btn-sm`} onClick={() => { this.confirmEvent() }}>
                        {confirm_button_text}
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
