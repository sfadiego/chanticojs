import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
import * as actions from '../Core/Store/actions/index'
import * as GLOBAL from '../Core/Constantes'
import Aux from '../Layout/AuxComponent';
class Perfil extends Component {

    render() {
        let userInfo = this.props.userData
        return (
            <li className="nav-item dropdown no-arrow">
                <Link className="nav-link dropdown-toggle" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" to={"#"}>
                    <span className="mr-2 d-none d-lg-inline text-gray-600 small">{`${userInfo.userFirstName} ${userInfo.userLastName}`}</span>
                    <FontAwesomeIcon className="img-profile rounded-circle" icon="user"></FontAwesomeIcon>
                </Link>

                <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    {
                        this.props.role === GLOBAL.CAT_ROLES.ADMINISTRADOR ?
                            <Aux>
                                <Link className="dropdown-item" to={"/cerrarcaja"}>
                                    <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="money-bill-alt"></FontAwesomeIcon> Corte de caja
                                </Link>
                                <div className="dropdown-divider"></div>
                            </Aux>
                            : null
                    }
                    <Link to="/logout" className="dropdown-item">
                        <FontAwesomeIcon className="fa-sm fa-fw mr-2 text-gray-400" icon="sign-out-alt"></FontAwesomeIcon>  Cerrar sesion
                    </Link>
                </div>
            </li>
        );
    }
}

const mapStateToProps = state => {
    return {
        userData: state.auth,
        role: parseInt(state.auth.role)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logoutEvent: () => dispatch(actions.authLogOut())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Perfil);