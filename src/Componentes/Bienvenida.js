import React from 'react'
import Logo from '../Resources/img/logo_chantico_secundario.png'

const componente = (props) => {
    return <div className="container-fluid pb-3">
        <div className="row">
            <div className="col-md-12 text-center">
                <img className="img-fluid img-comanda-logo" alt="imagen empresa" src={Logo} ></img>
            </div>
        </div>
        <div className="row mt-3">
            <div className="col-md-12 text-center">
                <h1 className="text-primary">Bienvenido </h1>
            </div>
        </div>
    </div>
}
export default componente;