import React, { Component } from 'react';
import {connect } from 'react-redux'
import { Redirect } from 'react-router';
import * as actions from '../Core/Store/actions/index'
class Logout extends Component {
    componentDidMount(){
        this.props.logoutEvent();
    }
    render() {
        return <Redirect to="/"></Redirect>;
    }
}

const mapDispatchToProps = (dispatch)=>{
    return {
        logoutEvent: () => dispatch(actions.authLogOut())
    }
}
export default connect(null,mapDispatchToProps)(Logout);