import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../Elementos/Comandas/Styles/Styles.css';
import imagen from '../../Resources/img/logo_chantico.png';
import axios from 'axios'
import Form from './Form';
import LoadingComponent from '../Elementos/LoadingComponent';
import * as actions from '../Core/Store/actions/index'
import { Link } from 'react-router-dom';
import RegistroForm from './RegistroForm'
import * as GLOBAL from '../Core/Constantes'
import { updateObject } from '../Core/utility'
import { validarForm, validarAllInputs } from './ValidateForm'

export class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usuario: '',
            password: '',
            responsemsg: null,
            formErrors: {
                nombre: '',
                apellido: '',
                username: '',
                password: '',
                role: '',
                tienda: '',
            },
            showRegistro: false
        }
        this.changePassword = this.changePassword.bind(this);
        this.changeUser = this.changeUser.bind(this);
        this.onsubmitHandler = this.onsubmitHandler.bind(this);
    }

    signal = axios.CancelToken.source();
    componentWillUnmount = () => this.signal.cancel(`loginform`);
    onsubmitHandler = event => {
        try {
            event.preventDefault();
            this.dispatchAuth();
        } catch (error) {
            if (axios.isCancel(error)) { console.log(error.message); }
        }
    }

    dispatchAuth = async () => {
        this.props.onAuth(this.state.usuario, this.state.password);
    }

    changePassword = (password) => this.setState({ password: password })
    changeUser = (user) => this.setState({ usuario: user });
    handleChange = (event) => {
        event.preventDefault();
        let validacion = validarForm(event)
        let data = updateObject(this.state.formErrors, {
            nombre: validacion.nombre || '',
            apellido: validacion.apellido || '',
            username: validacion.username || '',
            role: validacion.role,
            tienda: validacion.tienda || '',
        })

        this.setState({ formErrors: data });
    }
    registrar = (event) => {
        event.preventDefault();
        let formValid = validarAllInputs(event)
        if (!formValid.valido) {
            return this.setState({ responsemsg: formValid.msg });
        } else if (formValid.valido) {
            let data = formValid.data;
            axios.post('usuario', data)
                .then(response => this.setState({ showRegistro: false, responsemsg: null }))
                .catch(error => {
                    let errorResponse = error.response.data.error
                    if (errorResponse.username.kind && errorResponse.username.kind === "unique") {
                        this.setState({ responsemsg: GLOBAL.UNIQUE_USERNAME, fullError: errorResponse })
                    }
                });
        }

    }
    render() {
        let form = null;
        if (!this.state.showRegistro) {
            form = <Form
                usuario={this.state.usuario}
                password={this.state.password}
                onchangepassword={this.changePassword}
                onchangeuser={this.changeUser}
                submit={this.onsubmitHandler}>
            </Form>
        }
        if (this.props.loading) { form = <LoadingComponent></LoadingComponent> }
        let errorMessage = null;
        if (this.props.error) {
            errorMessage = (
                <div className="text-danger text-center">
                    <p>{this.props.error.data.msg}</p>
                </div>
            );
        }
        if (this.state.responsemsg) {
            errorMessage = (
                <div className="text-danger text-center">
                    <p>{this.state.responsemsg}</p>
                </div>
            );
        }

        if (this.state.showRegistro) {
            form = <RegistroForm
                validation={this.state.formErrors}
                handleChange={this.handleChange}
                registrar={this.registrar}></RegistroForm>
        }
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6 col-lg-6 col-md-6">
                        <div className="card o-hidden border-0 shadow-lg my-5">
                            <div className="card-body p-0">
                                <div className="row">
                                    <div className="col-md-12 text-center mt-4">
                                        <img alt="loginimage" className="w-40" src={imagen}></img>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="p-5">
                                            <div className="text-center">
                                                <h1 className="h4 text-gray-900 mb-4">Bienvenido! </h1>
                                            </div>
                                            {errorMessage}
                                            {form}
                                            <hr></hr>
                                            <div className="text-center">
                                                <Link className="small" to="#">Olvidaste el password?</Link>
                                            </div>
                                            <div className="text-center">
                                                <button className="btn text-primary" onClick={() => this.setState({ showRegistro: !this.state.showRegistro })}>
                                                    {!this.state.showRegistro ? 'Registrarse' : 'Iniciar sesion'}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (usuario, password) => dispatch(actions.auth(usuario, password)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
