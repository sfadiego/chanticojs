import React from 'react'
import * as GLOBAL from '../Core/Constantes'

let Form = (props) => {
    return <form className="user" onSubmit={props.registrar} >
        <div className="form-group">
            <input type="text" name="tienda" className="form-control form-control-user" onChange={props.handleChange} placeholder="CHANTICO"></input>

            <span style={{ color: 'red' }}>{props.validation['tienda']}</span>
        </div>
        <div className="form-group">
            <input type="text" name="nombre" className="form-control form-control-user" onChange={props.handleChange} placeholder="Nombre"></input>
            <span style={{ color: 'red' }}>{props.validation['nombre']}</span>
        </div>
        <div className="form-group">
            <input type="text" name="apellido" className="form-control form-control-user" onChange={props.handleChange} placeholder="Apellido"></input>
            <span style={{ color: 'red' }}>{props.validation['apellido']}</span>
        </div>
        <div className="form-group">
            <input type="text" name="username" className="form-control form-control-user" onChange={props.handleChange} placeholder="Usuario"></input>
            <span style={{ color: 'red' }}>{props.validation['username']}</span>
        </div>
        <div className="form-group">
            <select name="role" className="form-control form-control-user" onChange={props.handleChange}>
                <option value=""> Rol de usuario </option>
                <option value={GLOBAL.CAT_ROLES.USUARIO}> EMPLEADO </option>
                <option value={GLOBAL.CAT_ROLES.ADMINISTRADOR}> ADMINISTRADOR </option>
            </select>
            <span style={{ color: 'red' }}>{props.validation['role']}</span>
        </div>
        <div className="form-group">
            <input type="password" name="password" className="form-control form-control-user" onChange={props.handleChange} placeholder="Password"></input>
            <span style={{ color: 'red' }}>{props.validation['password']}</span>
        </div>
        <button className="btn btn-primary btn-user btn-block">Guardar </button>
    </form>
}

export default Form;
