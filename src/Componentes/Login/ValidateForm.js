import * as VALIDATIONS from './validationmsg'

let validarNombre = (value) => {
    if (value === '') {
        return VALIDATIONS.REQUIRED
    }

    if (value.length < 5) {
        return VALIDATIONS.MIN_LENGTH
    }
}
let validarapellido = (value) => {
    if (value.length < 5) {
        return VALIDATIONS.MIN_LENGTH
    }
}
let validarUsername = (value) => {
    if (value.length < 5) {
        return VALIDATIONS.MIN_LENGTH
    }
    if (value === '') {
        return VALIDATIONS.REQUIRED
    }
}

let validarPassword = (value) => {
    if (value.length < 5) {
        return VALIDATIONS.MIN_LENGTH
    }
    if (value === '') {
        return VALIDATIONS.REQUIRED
    }
}
let validarRole = (value) => {
    let rol = parseInt(value)
    if (rol === '') {
        return VALIDATIONS.REQUIRED
    }
}

let validarTienda = (value) => {
    if (value.length < 5) {
        return VALIDATIONS.MIN_LENGTH
    }

    if (value === '') {
        return VALIDATIONS.REQUIRED
    }
}

export const validarAllInputs = (formObject) => {
    let form = formObject.target;
    let nombre = form.nombre.value;
    let apellido = form.apellido.value;
    let username = form.username.value;
    let password = form.password.value;
    let role = parseInt(form.role.value);
    let tienda = form.tienda.value;

    if (nombre !== '' && apellido !== '' && username !== '' && password !== '' && role && role !== '' && tienda !== '') {
        return {
            valido: true,
            msg: VALIDATIONS.REGISTRED_USER,
            data: {
                nombre,
                apellido,
                username,
                password,
                role,
                tienda,
            }
        }
    } else {
        return { valido: false, msg: VALIDATIONS.REQUIRE_FIELD_REGISTER }
    }
}

export const validarForm = (formObject) => {
    const { name, value } = formObject.target;
    let data = {
        nombre: '',
        apellido: '',
        username: '',
        password: '',
        role: '',
        tienda: ''
    }

    switch (name) {
        case 'nombre':
            data.nombre = validarNombre(value);
            return data;
        case 'apellido':
            data.apellido = validarapellido(value);
            return data
        case 'username':
            data.username = validarUsername(value);
            return data;
        case 'password':
            data.password = validarPassword(value);
            return data;
        case 'role':
            data.role = validarRole(value);
            return data
        case 'tienda':
            data.tienda = validarTienda(value);
            return data
        default: return false
    }

}


