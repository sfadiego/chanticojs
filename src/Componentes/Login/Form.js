import React, { Component } from 'react'

export default class Form extends Component {
    render() {
        return (
            <form className="user" onSubmit={this.props.submit}>
                <div className="form-group">
                    <input type="text" className="form-control form-control-user" value={this.props.usuario} onChange={(event) => this.props.onchangeuser(event.target.value) } placeholder="Usuario"></input>
                </div>
                <div className="form-group">
                    <input type="password" className="form-control form-control-user" value={this.props.password} onChange={(event) => this.props.onchangepassword(event.target.value)} placeholder="Password"></input>
                </div>
                <button className="btn btn-primary btn-user btn-block"> Iniciar sesion </button>
            </form>
        )
    }
}
