import React, { Component } from 'react';
import Head from './Head'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class TableComponent extends Component {
    constructor(props) {
        super(props);
        this.state ={
            columnas: props.columnas,
            data: null,
            nombre_item1: props.nombre_item1,
            nombre_item2: props.nombre_item2,
            isLoaded:props.isLoaded
        }
    }

    dropItem(obj) {
         let id = obj._id
         this.props.borrar(id);
    }

    editRowTable(obj){
        let id = obj._id
        this.props.editar(id);
    }
    
    
    render() {
        
        let data = (this.props.data) ? this.props.data : [];
        let celda = (this.props.columnas) ? this.props.columnas : [];
        let cargado = (this.props.isLoaded) ? this.props.isLoaded : false;
        return (<table className="table mt-3">
                    <Head celdas={celda} ></Head>
                    <tbody>
                        {
                            cargado ? 
                            data.map((item, key) => {
                                return (
                                    <tr key={key}>
                                        <td><center>{key + 1}</center></td>
                                        <td><center>{ (typeof this.props.nombre_item1 === "object") ? 
                                                    item[this.props.nombre_item1.item][this.props.nombre_item1.index][this.props.nombre_item1.child_name] : 
                                                    item[this.props.nombre_item1]
                                            }</center>
                                        </td>
                                        {
                                            this.props.nombre_item2 !== null ? 
                                            <td><center>{
                                                    (typeof this.props.nombre_item2 === "object") ? 
                                                        item[this.props.nombre_item2.item][this.props.nombre_item2.index][this.props.nombre_item2.child_name] : 
                                                        item[this.props.nombre_item2]
                                            }</center></td>: null
                                        }
                                        <td> 
                                            <center> 
                                                <button onClick={(e)=> this.editRowTable(item)} className='btn btn-primary btn-sm mr-1' >
                                                    <FontAwesomeIcon icon={"edit"}></FontAwesomeIcon>
                                                </button> 
                                                <button onClick={(e)=> this.dropItem(item)} className="btn btn-danger btn-sm mr-1">
                                                    <FontAwesomeIcon icon={"trash"}></FontAwesomeIcon>
                                                </button> 
                                            </center> 
                                        </td>
                                    </tr>
                                )

                            })
                            : <tr>
                                <td colSpan={celda.length}>
                                    <center>
                                        Cargando... <FontAwesomeIcon icon="spinner" size="lg" spin></FontAwesomeIcon>
                                    </center>
                                </td>
                            </tr>
                        }
                        
                    </tbody>
                </table>
        );
    }
}

export default TableComponent;