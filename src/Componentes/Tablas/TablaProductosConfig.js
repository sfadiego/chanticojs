// ['#', 'Producto', 'Categoria', '--'];
export function columnsCatProductos(){
    return [
        {
            name: '#',
            sortable: true,
            cell: row => `${row._id.substr(row._id.length - 5).toUpperCase()}`
        },
        {
            name: 'Producto',
            sortable: true,
            cell: row => `${row.producto.toUpperCase()}`
        },
        {
            name: 'Categoria',
            sortable: true,
            cell: row => `${row.categoria_id[0].categoria}`
        }
    ];
    
}