import React, { Component } from 'react';

class Head extends Component {
    render() {
        const celdas = (this.props.celdas) ? this.props.celdas : [];
        return (
           <thead>
                <tr>
                    { 
                        celdas.map((item,key)=>{
                            return (<th key={key} scope="col"><center>{item}</center></th>);
                        })
                    }
                </tr>
            </thead> 
        );
    }
}

export default Head;