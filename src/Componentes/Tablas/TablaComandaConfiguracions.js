import * as Funciones from '../Core/Funcions'

function convertToDate(date){
    return Funciones.formatDate(date);
}

export function createColumnsComandaActiva(){
    return [
        {
            name: 'Pedido',
            sortable: true,
            cell: row => `${row._id.substr(row._id.length - 5).toUpperCase()}`
        },
        {
            name: 'Nombre de mesa',
            selector: 'nombre_pedido',
            sortable: true,
            center: true,
        },
        {
            name: 'Total',
            sortable: true,
            center: true,
            cell: row =>{
                return `$ ${row.total}`
            }
        },
        {
            name: 'Subtotal',
            sortable: true,
            center: true,
            cell: row =>{
                return `$ ${row.subtotal}`
            }
        },
        {
            name: 'Descuento',
            sortable: true,
            center: true,
            cell: row =>{
                return `${row.descuento}%`
            }
        },
        {
            name: 'Creado',
            sortable: true,
            center: true,
            cell: row=> convertToDate(row.createdAt)
        },
    ]; 
}
