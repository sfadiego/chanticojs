import React, { Component } from 'react';
import { connect } from 'react-redux'
import Layout from './Componentes/Layout/Layout';
import Contenidos from './Componentes/Layout/Contenidos';
import LoginForm from './Componentes/Login/LoginForm';
import { Route, Switch, withRouter } from 'react-router';
import * as actions from './Componentes/Core/Store/actions/index'
class App extends Component {
    componentDidMount() {
        this.props.onTryAutoSignIn();
    }
    render() {
        let component = this.props.isAuthenticated
            ? <Contenidos></Contenidos>
            : <Route path="/" component={LoginForm}></Route>
        return (
            <Layout isAuthenticated={this.props.isAuthenticated}>
                <Switch>
                    {component}
                </Switch>
            </Layout>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignIn: () => dispatch(actions.authCheckState())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));