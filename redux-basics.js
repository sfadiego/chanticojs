const redux = require('redux');
const createStore = redux.createStore; //Holds state tree

const initialState  = {
    counter:0
}
// Reducer
//State siempre debe estar inicializado 
//Reducer solo regresa el state 
const rootReducer = (state = initialState, action)=>{ 
    if(action.type === 'INC_COUNTER'){
        return {
            ...state,
            counter:state.counter + 1
        };
    }

    if(action.type === 'ADD_COUNTER'){
        return {
            ...state,
            counter:state.counter + action.value
        }
    }
    return state;
}
// Store
const store = createStore(rootReducer);
console.log(store.getState());

// Subscription
store.subscribe(()=>{
    console.log('[subscription]',store.getState());
});

// Dispatch =  envio
// envia datos desde tu aplicación a tu store
store.dispatch({type: 'INC_COUNTER' });
store.dispatch({type: 'ADD_COUNTER', value:10});
console.log(store.getState());
